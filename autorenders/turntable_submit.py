#rez-env rez_arcade rez_arcade_ftrack -- python turntable_submit.py

import os
import subprocess


def submit_tt(shot_id, shotRoot, name):
    gaffer = "rez-env gafferDefault -- gaffer"
    ttscene = "/job/MGTO3/EPISODE/pipe/techtest/gaffer01/work/rupertt/gaffer/thumbnail_v006.gfr"
    cmd = gaffer+' dispatch -script '+ttscene+' -tasks Wedge -dispatcher Deadline -settings -dispatcher.frameRange \'"1001-1100"\' -dispatcher.framesMode 2'

    cmd += ' -variables.arcade_shot_id.value \'"'+shot_id+'"\' -variables.projectRootDirectory.value \'"'+shotRoot+'"\''
    cmd += ' -variables.shot.value \'"'+name+'"\''
    returned_value = subprocess.call( cmd, shell=True )  


import arcade_ftrack
import arcade

arcade_versions = arcade_ftrack.Versions()
geos = arcade_versions.get_all_latest("GEO") 

submited_sids= []

for geo in geos:
    sid = geo["shot_id"]
    if not sid in submited_sids:
        submited_sids.append(sid)
        name = geo["name"]
        pdir = geo["path"].split("/pub/")[0]+"/work/light/rupertt/gaffer"
        print sid, name, pdir
        submit_tt(str(sid), pdir, name)


#submit_tt("172", "/job/MGTO3/ASSET/char/billyBarnacle")