# gaffer_arnold env python lookdev_dispatcher.py

import Gaffer
import GafferScene
import GafferDeadline
import arcade_ftrack

script = Gaffer.ScriptNode()

script["fileName"].setValue( "/job/MGTO3/EPISODE/pipe/autorenders/LOOK_turntables/pub/LOOK_turntables_v001.gfr")

script.load()

def submit_to_farm(geoPath, lookPath, assetName, ftrack_id):
    #set geo:
    script["SceneReader"]["fileName"].setValue( geoPath )
    
    #set naming:
    script["variables"]["member3"]["value"].setValue(assetName)
    script['Set']['name'].setValue("ASSET_"+assetName.split("_")[0])

    #load lookdev:
    script.importFile( lookPath )
    look_node = script[assetName]
    look_node["in"].setInput(script["look_in"]["out"])
    script["look_out"]["in"].setInput(look_node["out"])

    #set ftrackid:
    script["ftrack_playable"]['ftrack_id'].setValue( ftrack_id )

    #dispatch:
    dispatcher = GafferDeadline.DeadlineDispatcher()
    dispatcher["framesMode"].setValue( dispatcher.FramesMode.CustomRange )
    dispatcher["frameRange"].setValue( "1001-1300" )
    with script.context():
        dispatcher.dispatch( [script["ftrack_playable"]] )

    #cleanup:
    script.removeChild(look_node)


def test_render():
    geoPath = '/job/MGTO3/ASSET/char/billyBarnacle/pub/GEO/billyBarnacle_Main_GEO/v002/billyBarnacle_Main_GEO_v002.usd'
    lookPath = '/job/MGTO3/ASSET/char/billyBarnacle/pub/LOOK/billyBarnacle_LOOK/v001/billyBarnacle_LOOK_v001.gfr'
    assetName = 'billyBarnacle_LOOK'
    ftrack_id = 'cfad10b1-9f11-4299-803c-36d555865b25'
    submit_to_farm(geoPath, lookPath, assetName, ftrack_id)

#

def getAssets():
    ftrack_bridge = arcade_ftrack.Bridge()
    ftrack_bridge.connect()
    fts = ftrack_bridge.session

    #char:
    q = """AssetVersion where 
    asset.parent.parent.id = 43357428-1903-41c5-8ab0-d335d871a572
    and is_latest_version = True
    and asset.type.name = "LOOK"
    and custom_attributes any (key is "turntable" and value is True)
    """

    #building:
    q = """AssetVersion where 
    asset.parent.parent.id = 4a4a54e9-ad82-4122-a84f-f437a171d9cc
    and is_latest_version = True
    and asset.type.name = "LOOK"
    and custom_attributes any (key is "turntable" and value is True)
    """

    #set:
    q = """AssetVersion where 
    asset.parent.parent.id = fafe698f-0a95-4f12-ae2a-7a71363cdcb3
    and is_latest_version = True
    and asset.type.name = "LOOK"
    and custom_attributes any (key is "turntable" and value is True)
    """
    
    
    #prop, set, building, char:
    q = """AssetVersion where 
    asset.parent.parent.id in (fb372252-c8d8-4c38-ac20-35d255cd1261, fafe698f-0a95-4f12-ae2a-7a71363cdcb3, 4a4a54e9-ad82-4122-a84f-f437a171d9cc, 43357428-1903-41c5-8ab0-d335d871a572)
    and is_latest_version = True
    and asset.type.name = "LOOK"
    and custom_attributes any (key is "turntable" and value is True)
    """

    r = fts.query(q)
    versions = r.all()

    for v in versions:
        geoPath = None
        lookPath = None
        ftrack_id = v["id"]
        assetName = str(v["asset"]["name"])
        for c in v["components"]:
            if c["name"] == "gfr":
                lookPath = c["component_locations"][0]["resource_identifier"]
                break

        print lookPath
        print ftrack_id
        print assetName

        #get variants:
        variants = v["asset"]["parent"]["custom_attributes"]["variants"]
        print "##VAR: ", variants

        variant = "Main"
        if variants and not "Main" in variants:
            variant = variants.split(",")[0]

        #now get latest geo path.....
        q2 = """AssetVersion where 
        asset.parent.id = """+v["asset"]["parent"]["id"]+"""
        and is_latest_version = True
        and asset.type.name = "GEO"
        and asset.name like "%_"""+variant+"""_%"
        """
        r2 = fts.query(q2)
        geo_v = r2.first()
        if geo_v:
            for c2 in geo_v["components"]:
                if c2["name"] == "usd":
                    geoPath = c2["component_locations"][0]["resource_identifier"]
                    break
        print geoPath
        print

        if geoPath and lookPath:
            submit_to_farm(geoPath, lookPath, assetName, ftrack_id)
        
            v["custom_attributes"]["turntable"] = False
            fts.commit()
        #break


getAssets() 
#test_render() 