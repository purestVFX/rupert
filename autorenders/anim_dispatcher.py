# gaffer_arnold env python anim_dispatcher.py

import Gaffer
#import GafferScene
import GafferDeadline
import arcade_ftrack

script = Gaffer.ScriptNode()

script["fileName"].setValue( "/job/MGTO3/EPISODE/pipe/autorenders/ANIM_playables/work/Lighting/rupertt/gaffer/scripts/anim_playables_v001.gfr")

script.load()

def submit_to_farm(cachePath, camPath, assetName, ftrack_id, frameRange):
    #set geo:
    script["cache"]["fileName"].setValue( cachePath )

    #set cam:
    script["cam"]["fileName"].setValue( camPath )
    
    #set naming:
    script["variables"]["assetName"]["value"].setValue(assetName)

    #set ftrackid:
    script["ftrack_playable"]['ftrack_id'].setValue( ftrack_id )

    shotRoot = "/job/MGTO3/TEMP/anim_playables/"+ftrack_id+"/"
    #set path:
    script["variables"]["shotRoot"]["value"].setValue(shotRoot)

    #dispatch:
    dispatcher = GafferDeadline.DeadlineDispatcher()
    dispatcher["framesMode"].setValue( dispatcher.FramesMode.CustomRange )
    dispatcher["frameRange"].setValue( frameRange )
    with script.context():
        dispatcher.dispatch( [script["ftrack_playable"]] )





def getAssets():
    ftrack_bridge = arcade_ftrack.Bridge()
    ftrack_bridge.connect()
    fts = ftrack_bridge.session

    #301_TMF:
    q = """AssetVersion where 
    asset.parent.parent.parent.id = d86d1629-ab46-44e4-9cce-f561690ac6f6
    and is_latest_version = True
    and asset.type.name = "ANIM"
    and custom_attributes any (key is "turntable" and value is True)
    """

    r = fts.query(q)
    versions = r.all()

    for v in versions:
        hstart = int(v["asset"]["parent"]["custom_attributes"]["hstart"])
        hend = int(v["asset"]["parent"]["custom_attributes"]["hend"])
        fstart = int(v["asset"]["parent"]["custom_attributes"]["fstart"])
        fend = int(v["asset"]["parent"]["custom_attributes"]["fend"])
        
        if hstart and hend:
            frameRange = str(hstart)+"-"+str(hend)
        else:
            frameRange = str(fstart)+"-"+str(fend)
        

        camPath = None
        cachePath = None
        ftrack_id = v["id"]
        assetName = str(v["asset"]["name"])
        for c in v["components"]:
            if c["name"] == "usd":
                cachePath = c["component_locations"][0]["resource_identifier"]
                break

        print 
        print cachePath
        print ftrack_id
        print assetName
        print frameRange


        #now get latest cam path.....
        q2 = """AssetVersion where 
        asset.parent.id = """+v["asset"]["parent"]["id"]+"""
        and is_latest_version = True
        and asset.type.name = "CAM"
        """
        r2 = fts.query(q2)
        cam_v = r2.first()
        if cam_v:
            for c2 in cam_v["components"]:
                if c2["name"] == "usd":
                    camPath = c2["component_locations"][0]["resource_identifier"]
                    break
        print camPath
        print

        if cachePath and camPath:
            submit_to_farm(cachePath, camPath, assetName, ftrack_id, frameRange)
            
            v["custom_attributes"]["turntable"] = False
            fts.commit()
       


getAssets() 