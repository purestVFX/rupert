# gaffer_arnold env python layout_dispatcher.py


import arcade_ftrack


def submit_to_farm(camPath, layPath, assetName, ftrack_id, seqName, shotName, wavPath, pltPath, frameRange, movPath):
    import Gaffer
    import GafferScene
    import GafferDeadline
    script = Gaffer.ScriptNode()

    script["fileName"].setValue( "/job/MGTO3/EPISODE/pipe/autorenders/LAY_playables/pub/LAY_playables_v002.gfr")

    script.load()

    #set cam:
    camSceneReader = GafferScene.SceneReader( "camSceneReader" )
    camSceneReader["fileName"].setValue( camPath )
    script.addChild(camSceneReader)

    script["camConnecter"]["in"].setInput(camSceneReader["out"])
    
    script['version_text']['text'].setValue( assetName )

    #load layout:
    script["layout"]["fileName"].setValue( layPath )

    #set ftrackid:
    script["ftrack_playable"]['ftrack_id'].setValue( ftrack_id )
    script["ftrack_playable"]['wavPath'].setValue( wavPath )

    #set seq/shot:
    script["variables"]["shot"]["value"].setValue( shotName )
    script["variables"]["sequence"]["value"].setValue( seqName )

    #plate (animatic)
    script['plate']['fileName'].setValue( pltPath.replace("%04d", "####") )

    #mov out path:
    script['ftrack_playable']['out_path'].setValue(movPath)

    #dispatch:
    dispatcher = GafferDeadline.DeadlineDispatcher()
    dispatcher["framesMode"].setValue( dispatcher.FramesMode.CustomRange )
    dispatcher["frameRange"].setValue( frameRange )
    with script.context():
        dispatcher.dispatch( [script["ftrack_playable"]] )

    #script.save()
    #cleanup:
    script.removeChild(camSceneReader)
    del script


# def test_render():
#     geoPath = '/job/MGTO3/ASSET/char/billyBarnacle/pub/GEO/billyBarnacle_Main_GEO/v002/billyBarnacle_Main_GEO_v002.usd'
#     lookPath = '/job/MGTO3/ASSET/char/billyBarnacle/pub/LOOK/billyBarnacle_LOOK/v001/billyBarnacle_LOOK_v001.gfr'
#     assetName = 'billyBarnacle_LOOK'
#     ftrack_id = 'cfad10b1-9f11-4299-803c-36d555865b25'
#     submit_to_farm(geoPath, lookPath, assetName, ftrack_id)

# #

def getAssets():
    ftrack_bridge = arcade_ftrack.Bridge()
    ftrack_bridge.connect()
    fts = ftrack_bridge.session

    q = """AssetVersion where 
    asset.parent.parent.parent.id = d86d1629-ab46-44e4-9cce-f561690ac6f6
    and is_latest_version = True
    and asset.type.name = "LAY"
    and custom_attributes any (key is "turntable" and value is True)
    and not asset.name like "%_0000_%"
    """

    r = fts.query(q)
    versions = r.all()

    for v in versions:
        camPath = None
        layPath = None
        jpgPath = None
        wavPath = None


        ftrack_id = v["id"]
        assetName = str(v["asset"]["name"])

        shotName = str(v["asset"]["parent"]["name"])
        seqName = str(v["asset"]["parent"]["parent"]["name"])

        print v["asset"]["parent"]["custom_attributes"]["fstart"]
        

        for c in v["components"]:
            if c["name"] == "usd":
                layPath = c["component_locations"][0]["resource_identifier"]
                break

        print ""
        print layPath
        print ftrack_id
        print assetName
        print shotName
        print seqName

        fstart = str(int(v["asset"]["parent"]["custom_attributes"]["fstart"]))
        fend = str(int(v["asset"]["parent"]["custom_attributes"]["fend"]))
        
        frameRange = fstart+"-"+fend


        #now get latest cam path.....
        q2 = """AssetVersion where 
        asset.parent.id = """+v["asset"]["parent"]["id"]+"""
        and is_latest_version = True
        and asset.type.name = "CAM"
        and asset.name like "%_render_%"
        """
        r2 = fts.query(q2)
        geo_v = r2.first()
        for c2 in geo_v["components"]:
            if c2["name"] == "usd":
                camPath = c2["component_locations"][0]["resource_identifier"]
                break

        print camPath
        
        #now get latest plate path.....
        q3 = """AssetVersion where 
        asset.parent.id = """+v["asset"]["parent"]["id"]+"""
        and is_latest_version = True
        and asset.type.name = "PLT"
        """
        r3 = fts.query(q3)
        plt_v = r3.first()
        for c3 in plt_v["components"]:
            if c3["name"] == "jpg":
                jpgPath = c3["component_locations"][0]["resource_identifier"]
            if c3["name"] == "wav": 
                wavPath = c3["component_locations"][0]["resource_identifier"]

        print jpgPath
        print wavPath
        movPath = layPath.replace(".usd", ".mp4")

        submit_to_farm(camPath, layPath, assetName, ftrack_id, seqName, shotName, wavPath, jpgPath, frameRange, movPath)
        
        v["custom_attributes"]["turntable"] = False
        fts.commit()

        #break


getAssets() 
#script.save()
#test_render() 