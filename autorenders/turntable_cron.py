#rez-env rez_arcade_ftrack -- python turntable_cron.py
import os
import arcade_ftrack
import time
import subprocess


def submit_tt(geopath, ftrackid, assetName, renderpath):
    gaffer = "rez-env gafferDefault -- gaffer"
    #ttscene = "/job/MGTO3/EPISODE/pipe/autorenders/GEO_turntables/work/Lighting/rupertt/gaffer/scripts/ANIM_turntables_v001.gfr"
    ttscene = "/job/MGTO3/EPISODE/pipe/autorenders/GEO_turntables/work/Lighting/rupertt/gaffer/scripts/GEO_turntables_v003.gfr"
    cmd = gaffer+' dispatch -script '+ttscene+' -tasks ftrack_playable -dispatcher Deadline -settings -dispatcher.frameRange \'"1001-1200"\' -dispatcher.framesMode 2'

    cmd += ' -variables.member1.value \'"'+geopath+'"\''
    cmd += ' -variables.member4.value \'"'+ftrackid+'"\''
    cmd += ' -variables.member3.value \'"'+assetName+'"\''
    cmd += ' -variables.member2.value \'"'+renderpath+'"\''
    returned_value = subprocess.call( cmd, shell=True )  






ftrack_bridge = arcade_ftrack.Bridge()
ftrack_bridge.connect()
fts = ftrack_bridge.session

q = """AssetVersion where 
asset.parent.parent.parent.id = c1607de1-acfe-4084-a92c-d342952afec5
and is_latest_version = True
and asset.type.name = "GEO"
and custom_attributes any (key is "turntable" and value is True)
"""

# q = """AssetVersion where 
# asset.parent.parent.id = 43357428-1903-41c5-8ab0-d335d871a572
# and is_latest_version = True
# and asset.type.name = "ANIM"
# and custom_attributes any (key is "turntable" and value is True)
# """


r = fts.query(q)
versions = r.all()

for v in versions:
    geopath = None
    ftrackid = v["id"]
    assetName = v["asset"]["name"]
    for c in v["components"]:
        if c["name"] == "usd":
            geopath = c["component_locations"][0]["resource_identifier"]
            break
    renderpath =  os.path.join("/job/MGTO3/TEMP/tt_renders/", assetName, str(int(time.time())))

    print geopath
    print ftrackid
    print assetName
    print renderpath

    submit_tt(geopath, ftrackid, assetName, renderpath)
    v["custom_attributes"]["turntable"] = False
    fts.commit()
    


    
#av = fts.query('AssetVersion').first()
#av = fts.get('AssetVersion', '365092d2-f7f5-4d53-82d5-141be25378cb')
#print av.keys()