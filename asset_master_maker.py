import arcade_ftrack
from pxr import Usd, UsdGeom


# rez env needed:
# rez-env rez_arcade_ftrack gaffer

ftrack_bridge = arcade_ftrack.Bridge()
ftrack_bridge.connect()
fts = ftrack_bridge.session

Location = fts.pick_location()
def path_from_asset(ft_asset, component_name):
    components = ft_asset["versions"][-1]["components"]
    for c in components:
        if c["name"] == component_name:
            return Location.get_filesystem_path(c)


asset_build_name = "flowerRound"
usd_ab_path = "/tmp/"+asset_build_name+".usd"

usd_stage = Usd.Stage.CreateNew(usd_ab_path)
new_thing = usd_stage.OverridePrim('/'+asset_build_name)


vset = new_thing.GetVariantSets().AddVariantSet("geoVariants")



asset_build = fts.query('Shot where name is "'+asset_build_name+'"').one()  #danger : this will match the wrong thing sometimes
print asset_build["name"]


defaultset = False

for thing in asset_build["assets"]:
    print "   ", thing["name"]
    usd_file_path = path_from_asset(thing, "usd")
    vset.AddVariant(thing["name"])
    vset.SetVariantSelection(thing["name"])
    with vset.GetVariantEditContext():
        new_thing.GetReferences().AddReference(usd_file_path)

    print usd_file_path

#vset.ClearVariantSelection()
usd_stage.GetRootLayer().Save()


## attempt to make layout usd file (just a usd referencing above usd) fails........ 
print usd_ab_path
layout_usd = Usd.Stage.CreateNew("/tmp/layout.usd")
new_thing = layout_usd.OverridePrim("/test")
new_thing.GetReferences().AddReference(usd_ab_path)
layout_usd.GetRootLayer().Save()
#


