import json
import os

def convert_vrscene(ldev_ma_path, ldev_json, vray_scene_path, abc_path, assetName):
    print assetName

    cmds.file(ldev_ma_path, o=1, f=1, prompt=0)
    
    cmds.file(abc_path, i=1, f=1, prompt=0)

    fh = open(ldev_json)
    ldev_data = json.load(fh)
    for shdrSG in ldev_data:
        if cmds.objExists(shdrSG):
            print ldev_data[shdrSG]
            try:
                cmds.sets(ldev_data[shdrSG], edit=1, forceElement=shdrSG)
            except:
                print "##### shape not found ##"
        else:
            print "didn't find SG group in scene -- ", shdrSG
    if cmds.objExists(assetName+"_Model"):
        cmds.select(assetName+"_Model")
    elif cmds.objExists(assetName+"__Model"):
        cmds.select(assetName+"__Model")
    else:
        print "####################### no asset!!"
        return
        
    cmds.file(vray_scene_path, es=1, f=1, type="V-Ray Scene", options="")
    
count = 0  
for adir in os.listdir("/job/MGTO3/okido2data/lookdevs/3Dbldng/"):
    count += 1
    
    print "\n\n\n", count, "\n\n\n"
    #if count > 4:
    #    break
    base_path = "/job/MGTO3/okido2data/lookdevs/3Dbldng/"+adir+"/"
    ldev_ma_path = base_path + "shaders.ma"
    ldev_json = base_path + "connections.json"
    vray_scene_path = base_path + "asset.vrscene"
    abc_path = base_path + "geo.abc"
    
    if os.path.exists(ldev_ma_path) and os.path.exists(ldev_json) and os.path.exists(abc_path):
    
        convert_vrscene(ldev_ma_path, ldev_json, vray_scene_path, abc_path, adir)
     
    convert_vrscene(ldev_ma_path, ldev_json, vray_scene_path, abc_path, adir)
    