import json
import arcade_ftrack
import os
from pxr import Usd, UsdGeom
import sys

# rez env needed:
# rez-env rez_arcade_ftrack gaffer

output_path = "/job/MGTO3/okido2data/layouts/"


layoutfile = "/mnt/okido2/ASSETS/3Denv/cafeInt/publish/layout/Layout/v006/Layout_v006.json"
#layoutfile = "/mnt/okido2/ASSETS/3Denv/city/publish/layout/Layout/v007/Layout_v007.json"
#layoutfile = "/mnt/okido2/ASSETS/3Denv/houseGarden/publish/layout/Layout/v006/Layout_v006.json"
#layoutfile = "/mnt/okido2/ASSETS/3Denv/

ftrack_bridge = arcade_ftrack.Bridge()
ftrack_bridge.connect()
fts = ftrack_bridge.session

def path_from_asset(ft_asset, component_name):
    components = ft_asset["versions"][-1]["components"]
    for c in components:
        if c["name"] == component_name:
            return c["component_locations"][0]["resource_identifier"].replace("\\","/").replace("O:/okido_two/","/mnt/okido2/")
            


def convert_layout(layoutfile, layoutname, output_path):
    usd_test_path = output_path+layoutname+".usd"


    test_stage = Usd.Stage.CreateNew(usd_test_path)


    fh = open(layoutfile)

    layout_data = json.load(fh)

    for asset_id in layout_data:
        print(asset_id)
        ft_asset = fts.get("Asset", asset_id)

        #print(layout_data[asset_id])

        print(ft_asset["name"])
        abc_path = path_from_asset(ft_asset, "model-abc")
        if abc_path:
            print(abc_path, os.path.exists(abc_path))
            if os.path.exists(abc_path):
                usd_path_absolute = os.path.join(output_path, "models", os.path.basename(abc_path)[:-3]+"usd")
                usd_path = os.path.join("./models", os.path.basename(abc_path)[:-3]+"usd")

                print usd_path
                if not os.path.exists(usd_path_absolute):
                    convert_cmd = "usdcat "+abc_path+" --out "+usd_path_absolute
                    os.system(convert_cmd)

            for instance in layout_data[asset_id]:
                print(instance.keys(), instance["transform"][0])
                x, y, z = instance["transform"][0]
                rx, ry, rz = instance["transform"][1]
                sx, sy, sz = instance["transform"][2]

                print(x, y, z)
                new_thing = test_stage.OverridePrim('/'+instance["name"])
                new_thing.GetReferences().AddReference(usd_path)

                UsdGeom.XformCommonAPI(new_thing).SetTranslate((x, y, z))
                UsdGeom.XformCommonAPI(new_thing).SetRotate((rx, ry, rz))
                UsdGeom.XformCommonAPI(new_thing).SetScale((sx, sy, sz))
            
            #break


    test_stage.GetRootLayer().Save()

#convert_layout(layoutfile, "cafeIntTest", output_path)

oki2 = fts.query('Project where name is "Okido_Two"').one()

print(oki2)
for child in oki2["children"]:
    if child["name"] == "ASSETS":
        oki2ASSETS = child
        break

for child in oki2ASSETS["children"]:
    if child["name"] == "3Denv":
        oki2_3Denv = child
        break

for envAB in oki2_3Denv["children"]:
    print(envAB["name"])
    for an_asset in envAB["assets"]:
        if an_asset["name"] == "Layout":
            json_path = path_from_asset(an_asset, "layout-json")
            print(json_path)
            convert_layout(json_path, envAB["name"], output_path)


