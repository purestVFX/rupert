import arcade_ftrack
import sys
import time

print >> sys.stderr, 'Bridge'
ftrack_bridge = arcade_ftrack.Bridge()

print >> sys.stderr, 'Connect'
ftrack_bridge.connect()

print >> sys.stderr, 'Session'
fts = ftrack_bridge.session

#q = 'select version from AssetVersion where asset.parent.parent.id is "bbe1d5c6-5212-409c-be29-4acd897ca220" and asset.type.name is "GEO"'
q = 'select children.assets.versions.version from Sequence where id is "bbe1d5c6-5212-409c-be29-4acd897ca220" and children.assets.type.name is "GEO"'
q = 'select children.assets.versions.version, children.assets.type.name from Sequence where id is "bbe1d5c6-5212-409c-be29-4acd897ca220"'

#q = 'Type'
#q = 'AssetVersion'

print >> sys.stderr, 'Running query'
r = fts.query(q)

print >> sys.stderr, 'Getting data'
sequences = r.all()

print(sequences)
count = 0

for shot in sequences[0]["children"]:
    for asset in shot["assets"]:
        #print asset["type"]["name"]
        for version in asset["versions"]:
            count += 1
            print version["version"]

print count

# with fts.auto_populating(True):
#     asset = assets[1]
#     print asset.items()
#     print asset['asset']["name"]

