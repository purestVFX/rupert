#rez-env OpenTimelineIO clique ffmpeg rez_arcade rez_arcade_ftrack -- python okido3_conform.py

import opentimelineio as otio
import subprocess
import os
import json
import clique

import arcade
import arcade_ftrack
import shutil

def ensureDir(path):
    aDir = os.path.dirname(path)
    if not os.path.exists(aDir):
        os.makedirs(aDir)


def frames_to_TC(frames):
    fps = 25
    h = int(frames / (fps*3600)) 
    m = int(frames / (fps*60)) % 60 
    s = int((frames % (fps*60))/fps) 
    ms = (frames % (fps*60) % 25) * (1000/fps)
    return ( "%02d:%02d.%03d" % ( m, s, ms))


def clipDetails(clip):
    cd = {"name": clip.name, "startframe": clip.range_in_parent().start_time.value,  "duration": clip.duration().value}
    return cd


def cutVideo(inputMov, outMov, sf, ef):
    stc = frames_to_TC(sf)
    etc = frames_to_TC(ef)
    outWav = outMov.replace(".mp4", ".wav")
    cmd = "ffmpeg -i {} -ss {} -to {} -vf scale=960x540 -c:v libx264 -crf 30 {}".format(inputMov, stc, etc, outMov)
    subprocess.call( cmd, shell=True )  


def splitToFrames(inputMov, outPath, sf):
    cmd = "ffmpeg -i {} -start_number {} {}".format(inputMov, sf, outPath)
    subprocess.call( cmd, shell=True )  

def extractWav(inputMov, outPath):
    cmd = "ffmpeg -i {} {}".format(inputMov, outPath)
    subprocess.call( cmd, shell=True )  


def getShots(edit_xml_path, epp_code):
    timeline = otio.adapters.read_from_file(edit_xml_path)
    video_track = timeline.video_tracks()[2]    # sequences
    sequences = []
    for clip in video_track.each_clip():
        cd = clipDetails(clip)
        #remap name
        cd["name"] = epp_code+cd["name"][4:6]
        sequences.append(cd)


    video_track = timeline.video_tracks()[0]
    #print("Seq     :    Name    :     duration      :    startframe")
    shots = []
    for clip in video_track.each_clip():
        #remap name
        
        cd = clipDetails(clip)
        cd["name"] = cd["name"][2:6]
        sf = cd["startframe"]
        ef = sf + cd["duration"]
        
        for seq in sequences:
            if seq["startframe"] <= sf and seq["startframe"]+seq["duration"] > sf:
                this_seq = seq["name"]
        
        cd["sequence"] = this_seq
        #print(this_seq + "     " + cd["name"] + "     " + str(cd["duration"])+ "               "+ str(sf)+"  "+str(ef)+"   "+frames_to_TC(sf)+"   "+frames_to_TC(ef))
        shots.append(cd)

    return sequences, shots


def make_seq_files(sequences, Root, edit_mp4_path, startFrame = 1011):
    for seq in sequences:
        print seq["name"]
        seq_root = os.path.join(Root, "sequences", seq["name"])
        seq_playable_path = os.path.join(seq_root, seq["name"]+"_0000.mp4")
        seq_frames_path = os.path.join(seq_root, "frames" ,seq["name"]+"_0000.%04d.jpg")
        seq["playable"] = seq_playable_path
        seq["frames"] = seq_frames_path
        seq["sf"] = int(startFrame)
        seq["ef"] = int(startFrame + seq["duration"] - 1)

        if not os.path.exists(seq_playable_path):
            ensureDir(seq_frames_path)

            sf = seq["startframe"]
            ef = sf + seq["duration"]
            cutVideo(edit_mp4_path, seq_playable_path, sf, ef)
            extractWav(seq_playable_path, seq_playable_path.replace(".mp4", ".wav"))
            splitToFrames(seq_playable_path, seq_frames_path, startFrame)
    return sequences

def make_shot_files(shots, Root, edit_mp4_path, startFrame = 1011):
    for shot in shots:
        print shot["sequence"], shot["name"]
        name = shot["sequence"]+"_"+shot["name"]
        shot_root = os.path.join(Root, "shots", name)
        shot_playable_path = os.path.join(shot_root, name+".mp4")
        shot_frames_path = os.path.join(shot_root, "frames", name+".%04d.jpg")
        shot["playable"] = shot_playable_path
        shot["frames"] = shot_frames_path
        shot["sf"] = int(startFrame)
        shot["ef"] = int(startFrame + shot["duration"] - 1)
 
        if not os.path.exists(shot_playable_path):
            ensureDir(shot_frames_path)
            sf = shot["startframe"]
            ef = sf + shot["duration"]
            
            cutVideo(edit_mp4_path, shot_playable_path, sf, ef)
            extractWav(shot_playable_path, shot_playable_path.replace(".mp4", ".wav"))
            splitToFrames(shot_playable_path, shot_frames_path, startFrame)
    return shots

def makeotio_seq_edit(shots):
    timeline = otio.schema.Timeline(name = "test")

    track = otio.schema.Track(name = "V1")
    timeline.tracks.append(track)

    for shot in shots:
        if shot["sequence"] == "TMF01":
            print shot["name"]
            print shot["sf"], shot["ef"], shot["frames"]
            clip = otio.schema.Clip()
            clip.name = shot["sequence"]+"_"+shot["name"]

            clip.media_reference = otio.schema.ExternalReference(
                target_url="file://"+shot["frames"].replace(r".%04d.", ".[1011-"+str(int(1010+shot["duration"]))+"]."),
                available_range=otio.opentime.TimeRange(
                    start_time=otio.opentime.RationalTime(0, 25),
                    duration=otio.opentime.RationalTime(shot["duration"], 25) 
                    )
                )

            clip.source_range = otio.opentime.TimeRange(
                start_time=otio.opentime.RationalTime(0, 25),
                duration=otio.opentime.RationalTime(shot["duration"], 25)
                )

            track.append(clip)

    otio.adapters.write_to_file(timeline, "/home/rupertt/test.otio")




Root = "/job/MGTO3/EPISODE/301_TMF/01_EDIT/exports/Conform/"
edit_xml_path = Root+"/301_TMF_v009_ShotNumbers.xml"
edit_mp4_path = Root+"/301_TMF_v009.mp4"

episode_name = "301_TMF"
episode_code = episode_name.split("_")[1]

sequences, shots = getShots(edit_xml_path, episode_code)

sequences = make_seq_files(sequences, Root, edit_mp4_path)
shots = make_shot_files(shots, Root, edit_mp4_path)

arcob = arcade.Arcade()
bridge = arcade_ftrack.Bridge(arcob.situation)
bridge.connect()
server_location = bridge.session_first_query('Location where name is "ftrack.server"')
disk_location = bridge.session_first_query('Location where name is "ftrack.unmanaged"')
ft_asset_type_plate = bridge.session_first_query('AssetType where name is "PLT"')

pid = arcob.db.get_project_id("MGTO3")
#work out the episode id:
ep = None
for episode in arcob.db.get_entities('episode', pid):
    if episode["name"] == "EPISODE/"+episode_name:
        ep = episode
        break

if not ep:
    print "episode not found"
else:
    arcob.set_situation(ep["id"], arcade_type='episode')

# for seq in sequences:
#     print seq["name"]
#     arcob.situation["sequence"] = seq["name"]
#     arcob.situation["shot"] = "0000"
#     for task in ["layout", "Lighting", "conform"]:
#         arcob.situation["task"] = task
#         arcob.db.push_structure()
#         arcob.fs.build_directories()
#         bridge.push_structure()
  
#     asset_name = arcob.situation["shot"] + "_PLT"
#     av = bridge.add_asset_version(asset_name, ft_asset_type_plate, "okido3_conform python script publish")

#     components = av["components"]
#     for c in components:
#         if c["name"] == 'ftrackreview-mp4':
#             fts.delete(c)

#     component = av.create_component(
#         path=seq["playable"],
#         data={'name': 'ftrackreview-mp4'},
#         location=server_location
#     )

#     component['metadata']['ftr_meta'] = json.dumps({
#         'frameIn': 0,
#         'frameOut': int(seq["duration"]),
#         'frameRate': 25,
#         'height': 960,
#         'width': 540
#     })

#     thumbnail_component = bridge.session.create_component(
#         seq["frames"].replace(r".%04d.", ".1011."),
#         dict(name='thumbnail'),
#         location=server_location
#     )
#     av['thumbnail'] = thumbnail_component
#     ft_shot = av['asset']['parent']
#     ft_shot['thumbnail'] = thumbnail_component
#     ft_shot["custom_attributes"]['fstart'] = seq["sf"]
#     ft_shot["custom_attributes"]['fend'] = seq["ef"]


#     arcob.situation["asset_type"] = "PLT"
#     arcob.situation["file_name"] = arcob.fs.resolve_name("file_name")+"_"+arcob.situation["asset_type"]
#     arcob.situation["padding"] = r"%04d"
#     arcob.situation["version"] = "v"+str(av["version"]).zfill(3)
#     arcob.situation["extension"]="jpg"

#     publishPath = arcob.fs.resolve_path("published_part_seq")

#     av.create_component(publishPath,
#         data={'name': "jpg"},
#         location=disk_location
#         )
    
#     frames_dest = clique.parse(publishPath+" [{sf}-{ef}]".format(**seq))
#     frames_source = clique.parse("{frames} [{sf}-{ef}]".format(**seq))
#     for aframe in frames_source.indexes:
#         fmt_str = "{head}"+str(aframe).zfill(4)+"{tail}"
#         src = frames_source.format(fmt_str)
#         dest = frames_dest.format(fmt_str)
#         ensureDir(dest)
#         shutil.copy(src, dest)

#     arcob.situation["extension"]="wav"
#     publishPath = arcob.fs.resolve_path("published_part")

#     av.create_component(publishPath,
#         data={'name': "wav"},
#         location=disk_location
#         )
#     shutil.copy(seq["playable"].replace(".mp4", ".wav"), publishPath)

#     bridge.session.commit()

for shot in shots:
    print shot["name"]
    arcob.situation["sequence"] = shot["sequence"]
    arcob.situation["shot"] = shot["name"]
    for task in ["layout", "Animation", "Lighting", "comp", "conform", "Blocking"]:
        arcob.situation["task"] = task
        arcob.db.push_structure()
        arcob.fs.build_directories()
        bridge.push_structure()
  
    # asset_name = arcob.situation["shot"] + "_PLT"
    # av = bridge.add_asset_version(asset_name, ft_asset_type_plate, "okido3_conform python script publish")

    # components = av["components"]
    # for c in components:
    #     if c["name"] == 'ftrackreview-mp4':
    #         fts.delete(c)

    # component = av.create_component(
    #     path=shot["playable"],
    #     data={'name': 'ftrackreview-mp4'},
    #     location=server_location
    # )

    # component['metadata']['ftr_meta'] = json.dumps({
    #     'frameIn': 0,
    #     'frameOut': int(shot["duration"]),
    #     'frameRate': 25,
    #     'height': 960,
    #     'width': 540
    # })

    # thumbnail_component = bridge.session.create_component(
    #     shot["frames"].replace(r".%04d.", ".1011."),
    #     dict(name='thumbnail'),
    #     location=server_location
    # )
    # av['thumbnail'] = thumbnail_component
    # ft_shot = av['asset']['parent']
    # ft_shot['thumbnail'] = thumbnail_component
    # ft_shot["custom_attributes"]['fstart'] = shot["sf"]
    # ft_shot["custom_attributes"]['fend'] = shot["ef"]
    # ft_shot["custom_attributes"]['hstart'] = shot["sf"]-10
    # ft_shot["custom_attributes"]['hend'] = shot["ef"]+10


    # arcob.situation["asset_type"] = "PLT"
    # arcob.situation["file_name"] = arcob.fs.resolve_name("file_name")+"_"+arcob.situation["asset_type"]
    # arcob.situation["padding"] = r"%04d"
    # arcob.situation["version"] = "v"+str(av["version"]).zfill(3)
    # arcob.situation["extension"]="jpg"

    # publishPath = arcob.fs.resolve_path("published_part_seq")

    # av.create_component(publishPath,
    #     data={'name': "jpg"},
    #     location=disk_location
    #     )
    
    # frames_dest = clique.parse(publishPath+" [{sf}-{ef}]".format(**shot))
    # frames_source = clique.parse("{frames} [{sf}-{ef}]".format(**shot))
    # for aframe in frames_source.indexes:
    #     fmt_str = "{head}"+str(aframe).zfill(4)+"{tail}"
    #     src = frames_source.format(fmt_str)
    #     dest = frames_dest.format(fmt_str)
    #     ensureDir(dest)
    #     shutil.copy(src, dest)

    # arcob.situation["extension"]="wav"
    # publishPath = arcob.fs.resolve_path("published_part")

    # av.create_component(publishPath,
    #     data={'name': "wav"},
    #     location=disk_location
    #     )
    # shutil.copy(shot["playable"].replace(".mp4", ".wav"), publishPath)

    bridge.session.commit()






