ass_path = "/mnt/apps/TEMP_PROJECT_ROOT/Okido_Three_Testing/ASSETS/set/flowerRound/pub/LOOK/flowerRound_main_LOOK/v002/flowerRound_main_LOOK_v002.ass"



from arnold import *
AiBegin()
AiMsgSetConsoleFlags(AI_LOG_ALL)
AiASSLoad(ass_path, AI_NODE_ALL)
node = AiNodeLookUpByName("Green_Parameter")
#node = AiNodeLookUpByName("FlowerRound_master_MAF_v001Shape/input_merge_op")

entry = AiNodeGetNodeEntry( node )
it  = AiNodeEntryGetParamIterator( entry )
while not AiParamIteratorFinished(it):
    paramEntry = AiParamIteratorGetNext(it)
    param_name  = AiParamGetName( paramEntry )
    param_type  = AiParamGetType( paramEntry )
    is_linked =  AiNodeIsLinked(node, param_name)
    print "\n"
    print "name: ", param_name, param_type, " linked: ", is_linked

    if param_type == AI_TYPE_BOOLEAN:
        bool_value = AiNodeGetBool(node, param_name)
        print "boolean: ", bool_value
    elif param_type == AI_TYPE_STRING:
        string_value = AiNodeGetStr(node, param_name)
        print "string: ", string_value
    elif param_type == AI_TYPE_ARRAY:
        array_value = AiNodeGetArray(node, param_name)
        print "array: ", array_value

        #for i in range(AiArrayGetNumElements(array_value)):
        #    shader = AiArrayGetPtr(array_value, i)
        #    print AiNodeGetName(shader)
        
        #for thing in array_value:
        #    print "?: ", thing