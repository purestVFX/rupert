#rez-env rez_arcade_ftrack -- python migration_debug.py

import arcade_ftrack
ftrack_bridge = arcade_ftrack.Bridge()
ftrack_bridge.connect()
fts = ftrack_bridge.session

q = 'Asset where custom_attributes any (key is "migration info" and value is_not "")'

r = fts.query(q)
assets = r.all()

count = 0

for anAsset in assets:
    count += 1
    migration_info = anAsset["custom_attributes"]["migration info"]
    print anAsset["name"], migration_info

    S3_shot = fts.get("Shot", migration_info.split(":")[0])
    shotAssets = S3_shot["assets"]
    print "   ", S3_shot["name"]
    for shotAss in shotAssets:
        print "      ", shotAss["name"]

    if count > 20:
        break
