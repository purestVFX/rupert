#rez-env rez_arcade rez_arcade_ftrack -- python oki_conform3.py

import os
import re

import shutil
import subprocess
import tempfile

import arcade
import arcade_ftrack

import json





def ensureDir(path):
    aDir = os.path.dirname(path)
    if not os.path.exists(aDir):
        os.makedirs(aDir)



def get_fileDict(conform_path):

    fileList = os.listdir(conform_path)
    fileList.sort()

    fileDict = {}
    for afile in fileList:

        #print afile
        if re.match("[A-Z]{3}\d{2}_\d{4}[a-z]?\.(mov|nk|wav|mp4)", afile):
            shotName, ext = afile.split(".")
            if shotName in fileDict:
                fileDict[shotName].append(ext)
            else:
                fileDict[shotName] = [ext]
    return fileDict


def frameRangeFromNk(nukePath):
    ff = lf = ""
    if os.path.exists(nukePath):
        fh = open(nukePath)
        line = fh.readline()
        while line:
            if "last_frame" in line:
                lf = line.split()[1].strip()
            if "first_frame" in line:
                ff = line.split()[1].strip()
            if lf.isdigit() and ff.isdigit():
                lf = str(int(lf) - int(ff) + 1001)
                ff = "1001"
                return ff,lf
            line = fh.readline()


def conform(fileDict, conform_path):
    shotnames = fileDict.keys()
    shotnames.sort()
    count = 0

    arcob = arcade.Arcade()
    bridge = arcade_ftrack.Bridge(arcob.situation)
    bridge.connect()
    server_location = bridge.session_first_query('Location where name is "ftrack.server"')
    ft_asset_type_plate = bridge.session_first_query('AssetType where name is "PLT"')

    ep_arcade_id = 29

    for shotName in shotnames:

        sequence, shot = shotName.split("_")
        if 1:#count < 1:  # use this to continue a interupted conform
            

            nukeFile = conform_path + shotName + ".nk"
            movpath = conform_path + shotName + ".mp4"
            jpgpath = conform_path + shotName + ".jpg"

            ff, lf = frameRangeFromNk(nukeFile)   
            print sequence, shot, "  framerange:", ff, lf


            arcob.set_situation(ep_arcade_id, arcade_type='episode')

            arcob.situation["sequence"] = sequence
            arcob.situation["shot"] = "0000"
            for task in ["layout", "Lighting"]:
                arcob.situation["task"] = task
                arcob.db.push_structure()
                arcob.fs.build_directories()
                bridge.push_structure()

            arcob.situation["shot"] = shot
            for task in ["layout", "Animation", "Lighting", "comp", "conform"]:
                arcob.situation["task"] = task
                arcob.db.push_structure()
                arcob.fs.build_directories()
                bridge.push_structure()

            asset_name = shot + "_PLT"
            av = bridge.add_asset_version(asset_name, ft_asset_type_plate, "oki_conform3 python script publish")

            components = av["components"]
            for c in components:
                if c["name"] == 'ftrackreview-mp4':
                    fts.delete(c)

            component = av.create_component(
                path=movpath,
                data={'name': 'ftrackreview-mp4'},
                location=server_location
            )

            component['metadata']['ftr_meta'] = json.dumps({
                'frameIn': 0,
                'frameOut': int(lf)-int(ff),
                'frameRate': 25,
                'height': 1980,
                'width': 1080
            })


            thumbnail_component = bridge.session.create_component(
                jpgpath,
                dict(name='thumbnail'),
                location=server_location
            )
            av['thumbnail'] = thumbnail_component
            ft_shot = av['asset']['parent']
            ft_shot['thumbnail'] = thumbnail_component
            ft_shot["custom_attributes"]['fstart'] = int(ff)
            ft_shot["custom_attributes"]['fend'] = int(lf)

            


            bridge.session.commit()

            count += 1
                



# arcob = arcade.Arcade()
# bridge = arcade_ftrack.Bridge(arcob.situation)
# bridge.connect()
# server_location = bridge.session_first_query('Location where name is "ftrack.server"')
# ft_asset_type_plate = bridge.session_first_query('AssetType where name is "PLT"')

# ep_arcade_id = 9
# arcob.set_situation(ep_arcade_id, arcade_type='episode')

# arcob.situation["sequence"] = "HON05"
# arcob.situation["shot"] = "0180"
# arcob.situation["task"] = "layout"

# testfile_root = "/job/MGTO3/EPISODE/216_HON/01_EDIT/exports/conform/2017_10_13/"
# testfile_mp4_path = testfile_root + "HON05_0180.mp4"
# testfile_wav_path = testfile_root + "HON05_0180.wav"
# testfile_jpg_path = testfile_root + "HON05_0180.jpg"

# arcob.db.push_structure()
# bridge.push_structure()
# arcob.fs.build_directories()

# # ft_task = bridge.get_ftrack_entity_from_arcade(arcob.situation.at['task']['id'], 'task')
# # print ft_task

# asset_name = 

# av = bridge.add_asset_version("animatic", ft_asset_type_plate, "description")

# ## delete existing review components
# components = av["components"]
# for c in components:
#     if c["name"] == 'ftrackreview-mp4':
#         fts.delete(c)

# component = av.create_component(
#     path=testfile_mp4_path,
#     data={'name': 'ftrackreview-mp4'},
#     location=server_location
# )

# component['metadata']['ftr_meta'] = json.dumps({
#     'frameIn': 0,
#     'frameOut': 321,
#     'frameRate': 25,
#     'height': 1980,
#     'width': 1080
# })


# thumbnail_component = bridge.session.create_component(
#     testfile_jpg_path,
#     dict(name='thumbnail'),
#     location=server_location
# )
# av['thumbnail'] = thumbnail_component
    

# bridge.session.commit()



conform_path = "/job/MGTO3/EPISODE/216_HON/01_EDIT/exports/conform/2017_10_13/"
fileDict = get_fileDict(conform_path)
conform(fileDict, conform_path)



#ffmpeg -i HON06_0230.mp4 -start_number 1001 out.%04d.png