#rez-env rez_arcade_ftrack -- python okido2_lookdevs.py
import os
import arcade_ftrack
from shutil import copyfile

ftrack_bridge = arcade_ftrack.Bridge()
ftrack_bridge.connect()
fts = ftrack_bridge.session

q = 'select parent, parent.parent, latest_version.components from Asset where name is "Main" and type.name is "Lookdev" and parent.parent.parent_id is 20a3e960-3a29-11e7-8727-22000ab8015b'

r = fts.query(q)
assets = r.all()

count = 0
outroot = "/job/MGTO3/okido2data/lookdevs/"
print len(assets)

for anAsset in assets:
    count += 1

    
    cat_name = anAsset["parent"]["parent"]["name"]
    asset_name = anAsset["parent"]["name"]
    outpath = outroot + cat_name+"/"+asset_name+"/"

    print ""
    print count
    print cat_name, asset_name
    print outpath
    if not os.path.exists(outpath):
        os.makedirs(outpath)

    model_abc = None
    for otherAsset in anAsset["parent"]["assets"]:
        if otherAsset["name"] == asset_name+"_Model":
            for c in otherAsset["latest_version"]["components"]:
                if c["name"] == "model-abc":
                    model_abc = c["component_locations"][0]["resource_identifier"].replace("\\","/").replace("O:/okido_two/","/mnt/okido2/")
                    print "   ", otherAsset["name"], model_abc
                    break
            break

    model_abc_out = outpath + "geo.abc"

    try:
        copyfile(model_abc, model_abc_out)
    except:
        print "       ####################################"
        print "         model_abc copy error"

    # if count>10:
    #     break

    # lookdev_ma = None
    # lookdev_json = None
    # for c in anAsset["latest_version"]["components"]:
        
    #     if c["name"].startswith("lookdev-"):
    #         if c["name"] == "lookdev-ma":
    #             lookdev_ma = c["component_locations"][0]["resource_identifier"].replace("\\","/").replace("O:/okido_two/","/mnt/okido2/")
    #         elif c["name"] == "lookdev-json":
    #             lookdev_json = c["component_locations"][0]["resource_identifier"].replace("\\","/").replace("O:/okido_two/","/mnt/okido2/")

    
    # print "  ",lookdev_ma
    # print "  ",lookdev_json

    # lookdev_ma_out = outpath + "shaders.ma"
    # lookdev_json_out = outpath + "connections.json"

    # try:
    #     copyfile(lookdev_json, lookdev_json_out)
    # except:
    #     print "       ####################################"
    #     print "         json copy error"

    # try:
    #     fin = open(lookdev_ma, "rt")
    #     fout = open(lookdev_ma_out, "wt")
    #     #for each line in the input file
    #     for line in fin:
    #         #read replace the string and write to output file
    #         newline = line.replace("O:/okido_two/ASSETS/textures/","/job/MGTO3/okido2data/textures/")
    #         fout.write(newline)
    #     fin.close()
    #     fout.close()
    # except:
    #     print "       ####################################"
    #     print "         ma process error"

    


