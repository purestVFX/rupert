import os


okipedia_path="/home/rupertt/okipedia/season_one_episodes/"

header = """<!DOCTYPE html>
<html>
<head>
<style>

@font-face {
 font-family: okidoFont;
 src: url(../Okido-Medium.ttf);
}

#myInput {
  background-repeat: no-repeat;
  width: 500px;
  font-size: 16px;
  padding: 12px 20px 12px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
}

div.oki_ep{
   width: 280px; 
   height: 170px; 
   background-color: rgb(170,216,239); 
   text-align: center; 
   display:inline-block; 
   margin: 5px; 
   border-radius: 5px;
}

a{
   color: rgb(75,80,79);
}

</style>
<body style="text-align: center; font-family: okidoFont; color: rgb(244,63,39); ">
<Title>Okidopedia !!</Title>
<img src="../logo_2x.jpg" alt="okido logo"><p style="font-size: 50px;">Okidopedia !!</p>
"""

footer ="""
</body>
</html>
"""

index_text = header

def episode_page(assetName):
    html = header
    html += '<div style="position: fixed; left:10px; top:10px;"><a href="index.html"> BACK TO LIBRARY </a></div>'
    html+='<H1>'+assetName+'</H1>'
    html+='<video width="1280" height="720" controls><source src="media/'+assetName+'" type="video/mp4">Video not compatible</video>\n'

    html+=footer

    return html

def episode_html(assetName, movName, still=None):
    html = '<a href="'+assetName+'.html">'
    html +='<div class="oki_ep" id="'+assetName+'">'+ assetName
    html+='<br><video width="256" height="144"'
    if still:
        html+=' poster="media/stills/'+still+'"'

    html+='><source src="media/'+movName+'" type="video/mp4">Video not compatible</video>\n'
    html += "</div></a>\n"
    return html

index_text += "<br><br>"

asset_list = os.listdir(okipedia_path+"/media/")
stills_list = os.listdir(okipedia_path+"/media/stills/")
stills_dict = {}

for astill in stills_list:
    stills_dict[astill.split("_")[0]]=astill

asset_list.sort()

for asset in asset_list:
    if asset.startswith("1"):
        ep_name = asset.split("_Archive")[0].split("_v015_for_BBC")[0]
        shot_num = ep_name.split("_")[0]
        print(ep_name, asset)
        index_text += episode_html(ep_name, asset, stills_dict[shot_num])
        open(okipedia_path+"/"+ep_name+".html", 'wb').write(episode_page(asset))


index_text += footer

open(okipedia_path+"index.html", 'wb').write(index_text)