#rez-env rez_arcade_ftrack -- python ftrack_test.py

import arcade_ftrack
ftrack_bridge = arcade_ftrack.Bridge()
ftrack_bridge.connect()
fts = ftrack_bridge.session

ft_project_schemas = fts.query('ProjectSchema where name is EYE_Present').one()
for schema in ft_project_schemas:
    print schema["name"]