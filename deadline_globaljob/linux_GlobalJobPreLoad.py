import os
import sys
import json
import subprocess

from Deadline.Scripting import RepositoryUtils


os.environ['REZ_CONFIG_FILE'] = '/mnt/apps/config/.rezconfig'
sys.path.append('/mnt/apps/rez/lib/python2.7/site-packages')

from rez.resolved_context import ResolvedContext



def __main__(deadlinePlugin):
    def resolve_env():
        if os.environ.get('GLOBAL_JOB_PRELOAD_SKIP_RESOLVE') == 'True':
            deadlinePlugin.LogInfo('GlobalJobPreLoad: Skipping resolve...')
        else:
            packages = _get_requested_packages()
            _set_plugin_environment(packages)
            deadlinePlugin.LogInfo('GlobalJobPreLoad: Rezolve Done.')

    def alter_command_line(executable, arguments, working_directory):
        
        # make directory writable to other users
        os.system('chmod 777 -R "{}"'.format(deadlinePlugin.GetJobsDataDirectory()))

        if not executable.startswith('"') and not executable.startswith("'"):
            executable = '"{}"'.format(executable)

        job = deadlinePlugin.GetJob()
        user_cmd = '-E -u {}'.format(job.JobUserName)  # run as user, with preserved environment

        rez_context_path =
        rez_env_cmd = "rez-env --input {} -- ".format(_get_rez_context_path())

        if arguments:
            arguments = '{} {} {}'.format(user_cmd, rez_env_cmd, executable, arguments)
        else:
            arguments = '{} {}'.format(user_cmd, rez_env_cmd, executable)

        executable = 'sudo'

        print('----- Modified Command Line -----')
        print('Executable: "{}"'.format(executable))
        print('Arguments: "{}"'.format(arguments))
        print('Working Dir: "{}"'.format(working_directory))
        print('Data Dir: "{}"'.format(deadlinePlugin.GetJobsDataDirectory()))
        print('---------------------------------')

    return executable, arguments, working_directory

    def _get_rez_context_path():
        
        job = deadlinePlugin.GetJob()
        auxiliary = [aux for aux in job.AuxiliarySubmissionFileNames if aux == 'context.rxt']
        if auxiliary:
            job_path = '{}/jobs'.format(RepositoryUtils.GetRootDirectory())
            if os.path.exists(job_path):
                return os.path.join(job_path, job.JobId, auxiliary[0])
                

    # run methods
    deadlinePlugin.ModifyCommandLineCallback += alter_command_line
    resolve_env()