#rez-env OpenTimelineIO -- python otio_test.py

import opentimelineio as otio
import subprocess

Root = "/job/MGTO3/EPISODE/301_TMF/01_EDIT/exports/Conform/"
edit_xml_path = Root+"/301_TMF_v009_ShotNumbers.xml"
edit_mp4_path = Root+"/301_TMF_v008.mp4"

timeline = otio.adapters.read_from_file(edit_xml_path)


def frames_to_TC(frames):
    fps = 25
    h = int(frames / (fps*3600)) 
    m = int(frames / (fps*60)) % 60 
    s = int((frames % (fps*60))/fps) 
    ms = (frames % (fps*60) % 25) * (1000/fps)
    return ( "%02d:%02d.%03d" % ( m, s, ms))


def clipDetails(clip):
    cd = {"name": clip.name, "startframe": clip.range_in_parent().start_time.value,  "duration": clip.duration().value}
    return cd


def cutVideo(inputMov, outMov, sf, ef):
    stc = frames_to_TC(sf)
    etc = frames_to_TC(ef)
    cmd = "ffmpeg -i {} -ss {} -to {} -c:v libx264 -crf 30 {}".format(inputMov, stc, etc, outMov)
    subprocess.call( cmd, shell=True )  


video_track = timeline.video_tracks()[2]    # sequences
sequences = []
for clip in video_track.each_clip():
    sequences.append(clipDetails(clip))


video_track = timeline.video_tracks()[0]
print("Seq     :    Name    :     duration ")
for clip in video_track.each_clip():
    cd = clipDetails(clip)
    sf = cd["startframe"]
    ef = sf + cd["duration"]
    
    for seq in sequences:
        if seq["startframe"] <= sf and seq["startframe"]+seq["duration"] > sf:
            this_seq = seq["name"]
    print(this_seq + "     " + cd["name"] + "     " + str(cd["duration"]) ) #+ "               "+ str(sf)+"  "+str(ef)+"   "+frames_to_TC(sf)+"   "+frames_to_TC(ef))
    
    # outmov = Root +"shots/TMF"+this_seq[4:6]+"_"+cd["name"][2:]
    # print outmov
    # cutVideo(edit_mp4_path, outmov, sf, ef)






#ffmpeg -i 301_TMF_v008.mp4 -ss 00:40.000 -to 00:41.720 -c:v libx264 -crf 30 trim_opseek_encode.mp4

#ffmpeg -i HON06_0230.mp4 -start_number 1001 out.%04d.png