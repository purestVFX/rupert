import arcade_ftrack
ftrack_bridge = arcade_ftrack.Bridge()
ftrack_bridge.connect()
fts = ftrack_bridge.session
ft_location = fts.pick_location()

def okido2GeoPath(ft_asset):
    if ft_asset["versions"]:
        components = ft_asset["versions"][-1]["components"]
        for c in components:
            if c["name"] == "model-abc":
                path = path = c["component_locations"][0]["resource_identifier"]
                return path.replace("\\","/").replace("O:/okido_two/","/mnt/okido2/")

def okido2asset(assetNamePattern):
    q = "select versions.version from Asset where parent.parent.parent.id is 20a3e960-3a29-11e7-8727-22000ab8015b"
    q += " and name like '{}' and type.name is 'Geometry'".format(assetNamePattern)
    assets = fts.query(q)
    for asset in assets:
        print "name:   ", asset["name"].ljust(30), "id:  ", asset["id"]
        print "path:   ", okido2GeoPath(asset), "\n"

#### usage example:
#okido2asset("flowerRound%")