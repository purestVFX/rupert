#rez-env ffmpeg -- python encoding_stuff.py

import os
import subprocess
#import psutil
import time

#single pass contant quality:   
# 
# ffmpeg -i input.mp4 -c:v libvpx-vp9 -crf 30 -b:v 0 output.webm
#
#
#2-pass constant quality:
#
# ffmpeg -i input.mp4 -c:v libvpx-vp9 -b:v 0 -crf 30 -pass 1 -an -f null /dev/null && \
# ffmpeg -i input.mp4 -c:v libvpx-vp9 -b:v 0 -crf 30 -pass 2 -c:a libopus output.webm


def thumnail(input, output):
    cmd = "ffmpeg -y -loglevel quiet -i "+input+" -vframes 1 -f image2 "+output
    subprocess.Popen(cmd, shell=True)

def vp9_1_pass(input, output):
    cmd = "ffmpeg -y -loglevel quiet -i "+input+" -c:v libvpx-vp9 -crf 30 -b:v 0 "+output
    #os.system(cmd)
    subprocess.Popen(cmd, shell=True)

def vp9_2_pass(input, output):
    cmd = "ffmpeg -y -loglevel quiet -i "+input+" -c:v libvpx-vp9 -b:v 0 -crf 30 -pass 1 -an -f null /dev/null && ffmpeg -y -loglevel quiet -i "+input+" -c:v libvpx-vp9 -b:v 0 -crf 30 -pass 2 "+output
    #os.system(cmd)
    sp = subprocess.Popen(cmd, shell=True)
    #return sp.communicate()

def vp9_2_pass_1280(input, output):
    cmd = "ffmpeg -y -loglevel quiet -i "+input+" -vf scale=1280x720 -c:v libvpx-vp9 -b:v 0 -crf 30 -pass 1 -an -f null /dev/null && ffmpeg -y -loglevel quiet -i "+input+" -vf scale=1280x720 -c:v libvpx-vp9 -b:v 0 -crf 30 -pass 2 "+output
    cmd = "ffmpeg -y -i "+input+" -vf scale=1280x720 -c:v libvpx-vp9 -b:v 0 -crf 30 -pass 1 -an -f null /dev/null && ffmpeg -y -loglevel quiet -i "+input+" -vf scale=1280x720 -c:v libvpx-vp9 -b:v 0 -crf 30 -pass 2 "+output
    
    #os.system(cmd)
    sp = subprocess.Popen(cmd, shell=True)


def net_usage(inf = "eth0"):   #change the inf variable according to the interface
    net_stat = psutil.net_io_counters(pernic=True, nowrap=True)[inf]
    net_in_1 = net_stat.bytes_recv
    net_out_1 = net_stat.bytes_sent
    time.sleep(10)
    net_stat = psutil.net_io_counters(pernic=True, nowrap=True)[inf]
    net_in_2 = net_stat.bytes_recv
    net_out_2 = net_stat.bytes_sent

    net_in = round((net_in_2 - net_in_1) / 1310720.0, 3)
    net_out = round((net_out_2 - net_out_1) / 1310720.0, 3)

    return net_in+net_out

# tests:
# Infile = "/home/rupertt/encoding_tests/v003/ok_cmp_he09_0600_v003_DNxHD.mov"
# outfile1 = "/home/rupertt/encoding_tests/out_1_pass.mp4"
# outfile2 = "/home/rupertt/encoding_tests/out_2_pass.mp4"

# vp9_1_pass(Infile, outfile1)
# vp9_2_pass(Infile, outfile2)


#test_episode_source_H264 = "/mnt/sqst4/MESSY_GOES_TO_OKIDO/2_SERIES_TWO/EPISODES/03_Lower_Quality-1080p_h264/S2_Ep01_1080p_h264--Glow_Messy.mp4"

#series2_source = "/mnt/sqst4/MESSY_GOES_TO_OKIDO/2_SERIES_TWO/EPISODES/07_VP9/"
#out_dir= "/mnt/sqst4/MESSY_GOES_TO_OKIDO/2_SERIES_TWO/EPISODES/07_VP9/"

#series1_source = "/mnt/sqst4/MESSY_GOES_TO_OKIDO/1_SERIES_ONE/_Completed_Episode_Archive/VP9/"
#series1_source = "/mnt/sqst4/MESSY_GOES_TO_OKIDO/1_SERIES_ONE/_Completed_Episode_Archive/ProRes422HQ/"
#out_dir= "/mnt/sqst4/MESSY_GOES_TO_OKIDO/1_SERIES_ONE/_Completed_Episode_Archive/VP9/"

# episode_list = os.listdir(series1_source)
# for episode in episode_list:
#     if episode.endswith("HQ.mp4") and episode.startswith("1"):
        
#         src_mov = os.path.join(series1_source, episode)
#         dst_mp4 = os.path.join(out_dir, episode.replace(".mp4","_1280.mp4"))
#         if not os.path.exists(dst_mp4):
#             print()
#             print(episode)
#             print(src_mov)
#             print(dst_mp4)
#             print "## CPU : ", psutil.cpu_percent()
#             print "## network : ", net_usage()
#             while float(psutil.cpu_percent())>80 or net_usage() > 400:
#                 time.sleep(10)   #wait for the cpu to be calm


#             #output, error = vp9_2_pass(src_mov, dst_mp4) #do the encode
#             vp9_2_pass_1280(src_mov, dst_mp4) #do the encode
#             time.sleep(5)  #wait a bit befor looping
        



#vp9_1_pass(test_episode_source_H264, out_dir+"S2_Ep01--Glow_Messy_srcH264.mp4")
#vp9_1_pass(test_episode_source_DNxHD, out_dir+"S2_Ep01--Glow_Messy_srcDNxHD.mp4")

rootpath = "/job/MGTO3/EPISODE/216_HON/01_EDIT/exports/conform/2017_10_13/"

count = 0
fileList = os.listdir(rootpath)
fileList.sort()

for afile in fileList:

    if afile.endswith(".mov"):
        infile = rootpath + afile
        outfile = rootpath + afile[:-4]+".mp4"
        thumb = rootpath + afile[:-4]+".jpg"

        #thumnail(infile, thumb)
        vp9_2_pass(infile, outfile)
        time.sleep(30)

        print outfile
        

#infile = "/job/MGTO3/EPISODE/216_HON/01_EDIT/exports/conform/2017_10_13/HON05_0180.mov"
#outfile = "/job/MGTO3/EPISODE/216_HON/01_EDIT/exports/conform/2017_10_13/HON05_0180.mp4"
#thumb = "/job/MGTO3/EPISODE/216_HON/01_EDIT/exports/conform/2017_10_13/HON05_0180.jpg"

#thumnail(infile, thumb)
#vp9_2_pass(infile, outfile)