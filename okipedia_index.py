import os

okipedia_path="/home/rupertt/okipedia/vehicles/"
#okipedia_path="/home/rupertt/okipedia/characters/"
#okipedia_path="/home/rupertt/okipedia/props/"
#okipedia_path="/home/rupertt/okipedia/environments/"
#okipedia_path="/home/rupertt/okipedia/buildings/"
#okipedia_path="/home/rupertt/okipedia/set/"

header = """<!DOCTYPE html>
<html>
<head>
<style>

@font-face {
 font-family: okidoFont;
 src: url(../Okido-Medium.ttf);
}

#myInput {
  background-repeat: no-repeat;
  width: 500px;
  font-size: 16px;
  padding: 12px 20px 12px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
}

div.oki_asset{
   width: 146px; 
   height: 104px; 
   background-color: rgb(170,216,239); 
   text-align: center; 
   display:inline-block; 
   margin: 5px; 
   border-radius: 5px;
}

a{
   color: rgb(75,80,79);
}

</style>
<body style="text-align: center; font-family: okidoFont; color: rgb(244,63,39); ">
<Title>Okidopedia !!</Title>
<img src="../logo_2x.jpg" alt="okido logo"><p style="font-size: 50px;">Okidopedia !!</p>
"""

footer ="""
</body>
</html>
"""

index_text = header

def asset_page(assetName):
    html = header
    html += '<div style="position: fixed; left:10px; top:10px;"><a href="index.html"> BACK TO LIBRARY </a></div>'
    html+='<H1>'+assetName+'</H1>'
    html+='<video width="640" height="360" autoplay muted loop><source src="'+assetName+'/'+assetName+'.mp4" type="video/mp4">Video not compatible</video>\n<hr>\n'
    shot_examples_path = okipedia_path+assetName+"/shot_examples/"
    if os.path.exists(shot_examples_path):
        shot_examples = os.listdir(shot_examples_path)
        for vid in shot_examples:
            if vid.endswith(".mp4"):
                html += '<video width="640" height="359" controls><source src="'+assetName+'/'+'shot_examples/'+vid+'" type="video/mp4">Video not compatible</video>\n'
    else:
        html+="<br>No example shots found<br>"

    html+=footer

    return html


def asset_html(assetName):
    html = '<a href="'+assetName+'.html">'
    html +='<div class="oki_asset" id="'+assetName+'"><span style="position: relative; top: 3px;">'+ assetName +'</span>'
    html += '<br><img src = "'+ assetName +"/" + assetName +'.jpg" width="128" height="72" style="border-radius: 5px; position: relative; top: 6px;"></div></a>\n'
    return html

index_text +="""
<br>
<a href="../props/index.html">Props</a>   &nbsp;&nbsp;&nbsp;
<a href="../characters/index.html">Characters</a>    &nbsp;&nbsp;&nbsp;
<a href="../vehicles/index.html">Vehicles</a>     &nbsp;&nbsp;&nbsp;
<a href="../buildings/index.html">Buildings</a>     &nbsp;&nbsp;&nbsp;
<a href="../set/index.html">Set</a>     &nbsp;&nbsp;&nbsp;
<a href="../environments/index.html">Environments</a>
<br>

<br><input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.."><br>
"""


index_text +="""<script>
function
myFunction()
{

var
input, filter, a, i, txtValue;
input = document.getElementById('myInput');
filter = input.value.toUpperCase();

asset_divs = document.getElementsByClassName('oki_asset');


for (i = 0; i < asset_divs.length; i++) {

    a = asset_divs[i];


txtValue = a.id;
if (txtValue.toUpperCase().indexOf(filter) > -1) {
a.style.display = "inline-block";
} else {
a.style.display = "none";
}
}
}
</script>"""

asset_list = os.listdir(okipedia_path)
for asset in asset_list:
    if os.path.isdir(okipedia_path+asset):
        print asset
        index_text += asset_html(asset)
        open(okipedia_path+"/"+asset+".html", 'wb').write(asset_page(asset))


index_text += footer

open(okipedia_path+"index.html", 'wb').write(index_text)