#rez-env rez_arcade_ftrack -- python ftrack_component_edit.py

import arcade_ftrack
ftrack_bridge = arcade_ftrack.Bridge()
ftrack_bridge.connect()
fts = ftrack_bridge.session

#q = 'Asset where name is "Layout" and parent.name is "dressBushA"'

q = 'Asset where parent.parent.name is "land"'

count = 0
components = fts.query(q).all()
for a in components:
    count += 1
    print a["name"]
    for v in a["versions"]:
        for c in v["components"]:
            path = c["component_locations"][0]["resource_identifier"]
            print "   ", path
            if "/ASSET/Land/" in path:
                path = path.replace("/ASSET/Land/", "/ASSET/land/")
                c["component_locations"][0]["resource_identifier"] = path


    print count
    
fts.commit()