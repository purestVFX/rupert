
import ftrack_api
import ftrack_api.symbol
import requests
import os
import subprocess


session = ftrack_api.Session(
     server_url='https://squint-opera.ftrackapp.com',
     api_key='ff6c0728-1d4e-403c-9ec3-30d324b58861',
     api_user='rupertt'
)

okipedia_path="/home/rupertt/okipedia/"

#print session.types.keys()

print "#######################"
server_location = session.get('Location', ftrack_api.symbol.SERVER_LOCATION_ID)

#project = session.query('Project where name is "okido_two"').first()
#
#for child in project['children']:
#	print child["name"], child["id"]

#asset_folder = session.query('Folder where id is 20a3e960-3a29-11e7-8727-22000ab8015b').first()
#print asset_folder["name"]

#for child in asset_folder['children']:
#    print child["name"], child["id"], child["type"], child["object_type"]["name"]
#    print child.keys()

def ensure_dir(path):
    dir = os.path.dirname(path)
    if not os.path.exists(dir):
        os.makedirs(dir)
    return os.path.exists(dir)


def download(url, path):
    print url, path
    if url and path:
        if not os.path.exists(path):
            r = requests.get(url, allow_redirects=True)
            ensure_dir(path)
            open(path, 'wb').write(r.content)
        return True
    return False

def download_thum(thumbnail_id, path):
    thumbnail_component = session.get("Component", thumbnail_id)
    thumbnail_url = server_location.get_thumbnail_url(thumbnail_component, size=1280)
    r = requests.get(thumbnail_url, allow_redirects=True)
    open(path, 'wb').write(r.content)

def latest_lookdev_version(asset_build):
    for asset in asset_build["assets"]:
        if asset["name"] == "Main" and asset["type"]["name"] == "Lookdev":
            return asset["versions"][-1]
    return None

def latest_render_version(asset_build):
    for asset in asset_build["assets"]:
        #print asset["name"], asset["type"]["name"]
        if asset["type"]["name"] == "Render":
            return asset["versions"][-1]
    return None

def get_component_url(av, componetName):
    if av:
        for component in av["components"]:
            if component["name"] == componetName:
                if componetName == "thumbnail":
                    return server_location.get_thumbnail_url(component, size=256)
                else:
                    return server_location.get_url(component)
    return None

def encode_to_x265(inpath, outpath):
    #ffmpeg_cmd = "ffmpeg -y -i "+inpath+" -filter:v scale=640:360 -vcodec libx265 -crf 24 " + outpath + " ; rm "+inpath
    ffmpeg_cmd = "ffmpeg -y -i " + inpath + " -filter:v scale=640:360 " + outpath + " ; rm " + inpath

    print ffmpeg_cmd
    subprocess.Popen(ffmpeg_cmd, shell=True)

#inpath = "/home/rupertt/okipedia/zoomX/zoomX265.mp4"
#outpath = "/home/rupertt/okipedia/zoomX/zoomX265testtest.mp4"
#encode_to_x265(inpath, outpath)


# T3dEnv_cat = session.query('Category where id is 5e7cb924-5991-11e7-8e74-22000a7c13db').first()
# assetlist = T3dEnv_cat['children']
# okipedia_path += "environments/"

# T3dBuild_cat = session.query('Category where id is 43fabb14-5991-11e7-b90d-22000a7c13db').first()
# assetlist = T3dBuild_cat['children']
# okipedia_path += "buildings/"

T3dSet_cat = session.query('Category where id is 6df48efe-5991-11e7-b90d-22000a7c13db').first()
assetlist = T3dSet_cat['children']
okipedia_path += "set/"

#T3dProp_cat = session.query('Category where id is 69b65f70-5991-11e7-a1da-22000a7c13db').first()
#assetlist = T3dProp_cat['children']
#okipedia_path += "props/"

#T3dVeh_cat = session.query('Category where id is 72cca894-5991-11e7-b768-22000a7c13db').first()
#assetlist = T3dVeh_cat['children']
#okipedia_path += "vehicles/"

#T3dChar_cat = session.query('Category where id is 56cb2896-5991-11e7-b768-22000a7c13db').first()
#assetlist = T3dChar_cat['children']
#okipedia_path += "characters/"


max_shot_examples = 4
for assetBuild in assetlist:
#for assetBuild in T3dChar_cat['children']:
    asset_name = assetBuild["name"]
    path = okipedia_path + asset_name + "/"
    #ensure_dir(path)
    if 1:#asset_name.startswith("zoom"):
        print asset_name, assetBuild["object_type"]["name"]
        av = latest_lookdev_version(assetBuild)
        download(get_component_url(av, "ftrackreview-mp4"), path+asset_name+"_.mp4")
        encode_to_x265(path+asset_name+"_.mp4", path+asset_name+".mp4")
        download(get_component_url(av, "thumbnail"), path+asset_name + ".jpg")
        shot_examples = 0
        for outLink in assetBuild["outgoing_links"]:
            if shot_examples < max_shot_examples:
                linked = outLink["to"]
                lp = linked["parent"]["name"]
                if linked["object_type"]["name"] == "Shot" and not (lp.startswith("DR") or lp.startswith("char")) :

                    av = latest_render_version(linked)
                    print av
                    url = get_component_url(av, "ftrackreview-mp4")
                    if url:
                        semov_name = path +"/shot_examples/"+av["asset"]["name"][7:]
                        if download(url,  semov_name+"_.mp4"):
                            encode_to_x265(semov_name+"_.mp4", semov_name+".mp4")
                            shot_examples+=1
                        #download(get_component_url(av, "thumbnail"), path + asset_name +"/shot_examples/"+ ".jpg")



# zoom_bat_ab = session.query('AssetBuild where name is "zoomBat"').first()
# print zoom_bat_ab["name"], zoom_bat_ab["thumbnail_id"]
#
# print zoom_bat_ab.keys()
#
# av = latest_lookdev_version(zoom_bat_ab)
# for component in av["components"]:
#     print component["name"]
#     if component["name"] == "ftrackreview-mp4":
#         mp4url = server_location.get_url(component)



#for asset in zoom_bat_ab["assets"]:
#    print asset["name"], asset["type"]["name"]

#for outLink in zoom_bat_ab["outgoing_links"]:
#    linkedShot = outLink["to"]
#    if linkedShot["object_type"]["name"] == "Shot" and not linkedShot["parent"]["name"].startswith("DR"):
#        thumbnail_id = linkedShot["thumbnail_id"]
#        print linkedShot["name"], linkedShot["parent"]["name"], thumbnail_id

