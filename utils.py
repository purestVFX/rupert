import os
import subprocess
import psutil
import time



def vp9_1_pass(input, output):
    cmd = "ffmpeg -y -i "+input+" -c:v libvpx-vp9 -crf 30 -b:v 0 "+output
    #os.system(cmd)
    subprocess.Popen(cmd, shell=True)

def vp9_2_pass(input, output):
    cmd = "ffmpeg -y -i "+input+" -c:v libvpx-vp9 -b:v 0 -crf 30 -pass 1 -an -f null /dev/null && ffmpeg -y -i "+input+" -c:v libvpx-vp9 -b:v 0 -crf 30 -pass 2 "+output
    #os.system(cmd)
    sp = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return sp.communicate()

def net_usage(inf = "eth0"):   #change the inf variable according to the interface
    net_stat = psutil.net_io_counters(pernic=True, nowrap=True)[inf]
    net_in_1 = net_stat.bytes_recv
    net_out_1 = net_stat.bytes_sent
    time.sleep(1)
    net_stat = psutil.net_io_counters(pernic=True, nowrap=True)[inf]
    net_in_2 = net_stat.bytes_recv
    net_out_2 = net_stat.bytes_sent

    print net_in_1, net_in_2, net_out_1, net_out_2

    net_in = (net_in_2 - net_in_1) / 131072.0
    net_out = (net_out_2 - net_out_1) / 131072.0
    return net_in, net_out

