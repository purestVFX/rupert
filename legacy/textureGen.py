

from PIL import Image
from PIL import ImageDraw
import math
import random

#finds the straight-line distance between two points
def distance(ax, ay, bx, by):
    return math.sqrt((by - ay)**2 + (bx - ax)**2)

#rotates point `A` about point `B` by `angle` radians clockwise.
def rotated_about(ax, ay, bx, by, angle):
    radius = distance(ax,ay,bx,by)
    angle += math.atan2(ay-by, ax-bx)
    return (
        round(bx + radius * math.cos(angle)),
        round(by + radius * math.sin(angle))
    )


print "hello"



resolution = (8192,8192)
outpath = "C:\\Users\\rupertt\\crystals_8k_v2.png"
crycount = 400

crystals = []
while crycount:
    posx = int(random.random()*resolution[0])
    posy = int(random.random()*resolution[1])
    rotation = random.random()*3.4
    crystals.append([posx,posy,rotation])
    crycount -= 1


def crystal(posx, posy, scale=1.0, rotation=0.0):
    myshape = [(posx+scale,posy+scale), (posx+scale,posy-scale), (posx,posy-1.5*scale), (posx-scale,posy-scale), (posx-scale,posy+scale), (posx,posy+1.5*scale)]
    
    
    rotatedShape = []
    for point in myshape:
        rotatedShape.append(rotated_about(point[0],point[1],posx,posy,rotation))

    return rotatedShape


im = Image.new(mode = "RGB", size = resolution)

draw = ImageDraw.Draw(im, "RGB")

for s in range(1000, 1, -1):
    print s
    for c in crystals:
        colour = int(c[2]*75)
        draw.polygon(crystal(posx=c[0], posy=c[1], scale=(s/2.0), rotation=c[2]), fill=(colour,colour,colour))

im.save(outpath)