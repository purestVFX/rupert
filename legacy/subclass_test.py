
arcade = True

class Parent(object):
    def __init__(self):
        print "parent init ran"

    def test(self):
        parent_print("from parent object")


def parent_print(a_string):
    print "##############"
    print "parent printing function outside of class"
    print a_string
    print "##############"

