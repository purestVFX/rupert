import os
import re
import take
take.append_to_sys("sqLib")
take.append_to_sys("site_pkgs")
take.append_to_sys("lemonade")
from sqLib.ftrackLib import utils as ft_utils
import sqLib.pyLib.utils as pyUtils
from sqLib.pyLib import ffmpeg
import lemonade
from sqLib.ftrackLib import connect
session = connect.get_session()
import shutil
import clique
import subprocess
import tempfile
from PIL import Image
import Imath
import OpenEXR
import shutil


def ensureDir(path):
    aDir = os.path.dirname(path)
    if not os.path.exists(aDir):
        os.makedirs(aDir)



def get_episode(ep_code, project):
    q = "Episode where project.name is '"+project+"' and custom_attributes any (key is 'short_name' and value is '"+ep_code+"')"
    episodes = session.query(q).all()
    if not len(episodes):
        print "no episode found"
    elif len(episodes) > 1:
        print "multiple episodes have the same short code!"
    else:
        return episodes[0]
    return


def get_fileDict(conform_path):

    fileList = os.listdir(conform_path)
    fileList.sort()

    fileDict = {}
    for afile in fileList:

        #print afile
        if re.match("[A-Z]{2}\d{2}_\d{4}[a-z]?\.(mov|nk|wav|mp4)", afile):
            shotName, ext = afile.split(".")
            if shotName in fileDict:
                fileDict[shotName].append(ext)
            else:
                fileDict[shotName] = [ext]
    return fileDict

def createSequence(name, parent):
    #first check if it already exists:
    q = "Sequence where name is {} and parent_id is {}".format(name, parent["id"])
    ftSeq = session.query(q).first()
    if not ftSeq:
        data = {
            "parent": parent,
            "name": name
        }
        ftSeq = session.create("Sequence", data)
        session.commit()
    return ftSeq

def createShot(name, parent):
    #first check if it already exists:
    q = "Shot where name is {} and parent_id is {}".format(name, parent["id"])
    ftShot = session.query(q).first()
    if not ftShot:
        data = {
            "parent": parent,
            "name": name
        }
        ftShot = session.create("Shot", data)
        session.commit()
    return ftShot

def createAssetVersion(name, parent):
    #first check if it already exists:
    asset_type = session.query('AssetType where name is "plate"').one()

    q = "Asset where name is {} and parent.id is {}".format(name, parent["id"])
    ftAsset = session.query(q).first()
    if not ftAsset:
        data = {
            "name": name,
            "parent": parent,
            "type": asset_type
        }
        ftAsset = session.create("Asset", data)
        session.commit()

    latestVersion = ft_utils.get_latest_asset_version(session, ftAsset)
    if latestVersion:
        lv = latestVersion["version"]
    else:
        lv = 0

    asset_version = session.create("AssetVersion", {"asset": ftAsset, "version": lv+1})
    return asset_version


def frameRangeFromNk(nukePath):
    ff = lf = ""
    if os.path.exists(nukePath):
        fh = open(nukePath)
        line = fh.readline()
        while line:
            if "last_frame" in line:
                lf = line.split()[1].strip()
            if "first_frame" in line:
                ff = line.split()[1].strip()
            if lf.isdigit() and ff.isdigit():
                lf = str(int(lf) - int(ff) + 1001)
                ff = "1001"
                return ff,lf
            line = fh.readline()

def frameRangeFromMp4(vid_file_path):
    vid_info = ffmpeg.ffprobe(vid_file_path)
    for stream in vid_info["streams"]:
        if stream["codec_type"] == "video":
            fps = float(stream["r_frame_rate"].split("/")[0])
            duration = float(stream["duration"])
            ff = 1001
            lf = 1000+int(fps*duration)
            return ff, lf

def conform(fileDict, conform_path, lemon, first=0, last=100000):
    shotnames = fileDict.keys()
    shotnames.sort()
    for shotName in shotnames:

        ftypes = fileDict[shotName]
        episodeCode = shotName[:2]
        sequence, shot = shotName.split("_")
        if int(shot[:4]) > first and int(shot[:4]) < last:  # use this to continue a interupted conform
            ff = lf = False
            if len(ftypes) == 3 and "nk" in ftypes and "mov" in ftypes and "wav" in ftypes:

                nukeFile = conform_path + shotName + ".nk"
                ff, lf = frameRangeFromNk(nukeFile)

            elif len(ftypes) == 2 and "wav" in ftypes and "mp4" in ftypes:
                mp4File = conform_path + shotName + ".mp4"
                ff, lf = frameRangeFromMp4(mp4File)
                #frameRangeFromMp4(mp4File)
            ##########################

            #get frame range from mp4 here

            #########################


            if ff and lf:
                print episodeCode, sequence, shot, "  framerange:", ff, lf
                ftEpisode = get_episode(episodeCode, "okido_two")

                ftSequence = createSequence(sequence, ftEpisode)
                ftShot = createShot(shot, ftSequence)
                ftShot["custom_attributes"]["fstart"] = int(ff)
                ftShot["custom_attributes"]["fend"] = int(lf)#


                ftShot = session.get("Shot", ftShot["id"])

                ctx = lemon.context_from_entity(ftShot)
                fileTemplate = lemon.get_file_template(["part", "publish", "nuke", ctx])

                fields = lemon.template_fields_from_context(fileTemplate, ctx)

                ftAV = createAssetVersion("conform", ftShot)
                fields['version'] = "v"+str(ftAV["version"]).zfill(3)
                fields['type'] = "audio"
                fields['extension'] = "wav"
                fields['filename'] = shotName
                publishPath = lemon.path_from_template(fileTemplate, fields)
                print publishPath
                ensureDir(publishPath)
                shutil.copy(conform_path + shotName + ".wav", publishPath)

                ftAV.create_component(publishPath,
                                              data={'name': "audio-wav"},
                                              location=ft_utils.get_location(session))

                session.commit()

                if "mp4" in ftypes:
                    ftAV.encode_media(conform_path + shotName + ".mp4")
                else:
                    ftAV.encode_media(conform_path + shotName + ".mov")



            else:
                print "fail", fileDict

def convertImage(sourceImage, outputImage):
    file = OpenEXR.InputFile(sourceImage)
    pt = Imath.PixelType(Imath.PixelType.FLOAT)
    dw = file.header()['dataWindow']
    size = (dw.max.x - dw.min.x + 1, dw.max.y - dw.min.y + 1)
    rgbf = [Image.frombytes("F", size, file.channel(c, pt)) for c in "RGB"]
    def normalize_0_255(v):
        return (v * 755)
    rgb8 = [im.point(normalize_0_255).convert("L") for im in rgbf]
    Image.merge("RGB", rgb8).save(outputImage)


def makePlayable(render, assetVersion):
    import getpass
    from sqLib import deadlineLib
    deadline = deadlineLib.connect()

    JobInfo = {
        "Name": "Ftrack Make Playable Media " + os.path.basename(render),
        "UserName": getpass.getuser(),
        "Frames": "0",
        "Group": "all",
        "Pool": "qtime",
        "Plugin": "Python",
        "Department": "Comp",
        "Comment": "Produces a quick time movie from the provided content, "
                   "and uploads it to a pre existing AssetVersion"
    }

    PluginInfo = {
        "ScriptFile": take.get_path("playable_version_job"),
        "Arguments": '-r "{0}" -a {1} -u {2}'.format(render, assetVersion['id'], getpass.getuser()),
        "Version": "2.7"
    }
    deadline.Jobs.SubmitJob(JobInfo, PluginInfo)


def conform_liveaction(conform_path, lemon):
    filelist = os.listdir(conform_path)
    collections, remainder = clique.assemble(filelist)
    if len(collections) == 1:
        c = collections[0]
        #for collection in collections:
        head = c.head
        padding = c.padding
        tail = c.tail
        print len(head)
        if len(head) in (10, 11):
            print head, padding, tail
            shotName = head[:9]
            episodeCode = shotName[:2]
            seqence, shot = shotName.split("_")
            take = head[9].upper()
            if take == ".":
                take = "A"

            ftEpisode = get_episode(episodeCode, "okido_two")
            ftSequence = createSequence(seqence, ftEpisode)
            ftShot = createShot(shot, ftSequence)

            ctx = lemon.context_from_entity(ftShot)
            fileTemplate = lemon.get_file_template(["partseq", "publish", "nuke", ctx])
            fields = lemon.template_fields_from_context(fileTemplate, ctx)
            ftAV = createAssetVersion("plate"+take, ftShot)
            fields['version'] = "v" + str(ftAV["version"]).zfill(3)
            fields['type'] = "plate"
            fields['extension'] = "exr"
            fields['filename'] = shotName+"_plate"+take



            copy_service = pyUtils.get_copy_service()

            print episodeCode, seqence, shot, take

            for frame in c.indexes:
                originalFile = conform_path + head + str(frame).zfill(padding) + tail
                fields['padding'] = str(frame+1001)
                publishPath = lemon.path_from_template(fileTemplate, fields).replace("\\", "/")
                print originalFile
                print publishPath
                ensureDir(publishPath)



                if copy_service and not copy_service.closed:
                    copy_service.root.copy(originalFile, publishPath)
                else:
                    shutil.copy(originalFile, publishPath)

                convertImage(publishPath, publishPath.replace(".exr", ".jpg"))
            #hacky frame range string build:
            framerange = "[1001-"+fields['padding']+"]"
            fields['padding'] = "%04d"
            publishPath = lemon.path_from_template(fileTemplate, fields).replace("\\", "/")+" "+framerange

            ftAV.create_component(publishPath, data={'name': "sequence-exr"}, location=ft_utils.get_location(session))
            ftAV.create_component(publishPath.replace(".exr", ".jpg"), data={'name': "sequence-jpg"}, location=ft_utils.get_location(session))

            session.commit()

            makePlayable(publishPath.replace(".exr", ".jpg"), ftAV)

lemon = lemonade.Lemonade("O://OKIDO_TWO//")

#
#
liveaction_path = "O:/OKIDO_TWO/EPISODES/Hailstones/editorial/exports/plates/"
#
for adir in os.listdir(liveaction_path):
    path = liveaction_path + adir + "/"
    print "\n\n######################\n", path, "\n\n######################\n"
    conform_liveaction(path, lemon)




#conform_path = "O:/OKIDO_TWO/EPISODES/Touch/editorial/exports/conform/2017_08_08/"
#conform_path = "O:/OKIDO_TWO/EPISODES/Maps/editorial/exports/conform/2017_07_21/"
#conform_path = "O:/OKIDO_TWO/EPISODES/Phosphor/editorial/exports/conform/2017_07_20/"
#conform_path = "O:/OKIDO_TWO/EPISODES/Hearing/editorial/exports/conform/2017_07_19/"
#conform_path = "O:/OKIDO_TWO/EPISODES/Static/editorial/exports/conform/2017_08_07/"
#conform_path = "O:/OKIDO_TWO/EPISODES/Crystals/editorial/exports/conform/2017_08_16/"
#conform_path = "O:/OKIDO_TWO/EPISODES/Nocturnal/editorial/exports/conform/2017_08_09/"
#conform_path = "O:/OKIDO_TWO/EPISODES/Migration/editorial/exports/conform/2017_08_08/"
#conform_path = "O:/OKIDO_TWO/EPISODES/Phosphor/editorial/exports/conform/done/2017_06_21/"
#conform_path = "O:/OKIDO_TWO/EPISODES/Yeast/editorial/exports/conform/2017_08_17/"
#conform_path = "O:/OKIDO_TWO/EPISODES/Cones/editorial/exports/conform/2017_08_17/"
#conform_path = "O:/OKIDO_TWO/EPISODES/Eclipse/editorial/exports/conform/2017_08_18/"

#fileDict = get_fileDict(conform_path)
#conform(fileDict, conform_path, lemon)