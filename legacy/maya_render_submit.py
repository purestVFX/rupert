

startupScript ="""
import maya.cmds as cmds
cmds.file("Q:/EYEP_12_Xephos_and_Honeydew/ASSETS/3Dchar/test/publish/model/v009/test_model_v009.ma", i=True)
"""

import getpass
import take
take.append_to_sys("sqLib","production")
from sqLib import deadlineLib
deadline = deadlineLib.connect()

JobInfo = {
    "Name": "turntable test",
    "UserName": getpass.getuser(),
    "Frames": "1001-1071",
    "ChunkSize":"100000",
    "Group":"all",
    "Pool":"maya",
    "Plugin": "MayaBatch",
    "Comment": "testing auto turntable renders",
    "OutputDirectory0":"Q:/EYEP_12_Xephos_and_Honeydew/RenderData/rupertt/vray_scenes/"
}

PluginInfo = {
    "Animation":"1",
    "Renderer":"vrayExport",
    "VRayExportFile":"Q:/EYEP_12_Xephos_and_Honeydew/RenderData/rupertt/vray_scenes/auto_turntable.vrscene",
    "Version":"2016",
    "Build":"64bit",
    "ProjectPath":"q:/eyep_12_xephos_and_honeydew/ASSETS/3Dchar/test/work/rupertt/maya",
    "StartupScript":"Q:/EYEP_12_Xephos_and_Honeydew/RenderData/rupertt/test.mel",
    "ImageWidth":"1200",
    "ImageHeight":"674",
    "OutputFilePath":"q:/eyep_12_xephos_and_honeydew//RenderData/rupertt/images/",
    "OutputFilePrefix":"<Layer>/my_turntable_test",
    "Camera":"camera1",
    "SceneFile":"Q:/eyep_12_xephos_and_honeydew/ASSETS/3Dchar/test/work/rupertt/maya/scenes/turntable_v002.ma",
    "IgnoreError211":"0"
}

job = deadline.Jobs.SubmitJob(JobInfo, PluginInfo)
print "#######"
print job["_id"]
print "#######"


JobInfo = {
    "Name":"ttt_render",
    "Frames":"1001-1071",
    "ChunkSize": "10",
    "UserName": getpass.getuser(),
    "Group":"all",
    "Pool":"maya",
    "JobDependency0":job["_id"],
    "Plugin":"Vray",
    "OutputDirectory0":"q:/eyep_12_xephos_and_honeydew//RenderData/rupertt/images/"
}

PluginInfo = {
    "InputFilename":"Q:/EYEP_12_Xephos_and_Honeydew/RenderData/rupertt/vray_scenes/auto_turntable_turntable.vrscene",
    "SeparateFilesPerFrame":"0"
}
job = deadline.Jobs.SubmitJob(JobInfo, PluginInfo)

JobInfo = {
            "Name": "Ftrack Make Playable Media (turntable)",
            "UserName": getpass.getuser(),
            "Frames": "0",
            "Group":"all",
            "Pool":"nuke",
            "JobDependency0":job["_id"],
            "Plugin": "Python",
            "Department": "Comp",
            "Comment": "Produces a quick time movie from the provided content, "
                       "and uploads it to a pre existing AssetVersion"
        }
render = "q:/eyep_12_xephos_and_honeydew//RenderData/rupertt/images/turntable/my_turntable_test.%04d.png [1001-1071]"
PluginInfo = {
    "ScriptFile": take.get_path("playable_version_job","production"),
    "Arguments": '-r "{0}" -a {1} -u {2}'.format(render, "fef9de99-31d6-4b6b-bcaa-871d9a777a2a", getpass.getuser()),
    "Version": "2.7"
}

deadline.Jobs.SubmitJob(JobInfo, PluginInfo)