import logging

import ftrack_api
import ftrack_api.entity.location
import ftrack_api.accessor.disk
import ftrack_api.structure.origin


logging.basicConfig(level=logging.DEBUG)


def configure_locations(event):
    '''Configure locations for session.'''
    session = event['data']['session']

    location = session.query('Location where name is "my.location"').one()
    ftrack_api.mixin(location, ftrack_api.entity.location.UnmanagedLocationMixin)
    location.accessor = ftrack_api.accessor.disk.DiskAccessor(prefix='')
    location.structure = ftrack_api.structure.origin.OriginStructure()
    location.priority = -100


def register(session):
    '''Register plugin with *session*.'''
    session.event_hub.subscribe(
        'topic=ftrack.api.session.configure-location',
        configure_locations
    )
