import bpy


# ret_val = bpy.ops.import_scene.obj(filepath = "E:/Projects/blender/launch_script/plane.obj")

# obj_object = bpy.context.object
# obj_object = bpy.object.get("group1")
# obj_object.scale=((0.1,0.1,0.1))
# bpy.context.scene.render.engine = 'CYCLES'
# print("##################")
# print(ret_val)


### set render camera:
# context = bpy.context
# scene = context.scene
# currentCameraObj = bpy.data.objects[bpy.context.active_object.name]
# scene.camera = currentCameraObj

def getRootNodes(nodes):
    rootNodes = []
    for node in nodes:
        if (node.parent == None):
            rootNodes.append(node)
    return rootNodes


def abcImporter(path):
    # get current scene size:
    initialScene = bpy.data.objects
    sceneSize = len(initialScene)

    # do my import:
    bpy.ops.wm.alembic_import(filepath=path)

    # wait for scene size to change:
    elapsed = 0.0
    maxTime = 10
    while (sceneSize == len(bpy.data.objects) and elapsed < maxTime):
        time.sleep(0.2)
        elapsed += 0.2

    newNodes = set(initialScene).difference(set(bpy.data.objects))
    # return new nodes:
    return newNodes


###############################################################
# import env:
bpy.ops.wm.alembic_import(filepath="E:/Projects/blender/launch_script/xephos-room_Model_v001.abc")

# import alembic camera:
bpy.ops.wm.alembic_import(filepath="E:/Projects/blender/launch_script/RenderCam_v003.abc")

# set as render camera:
bpy.context.scene.camera = bpy.data.objects["RenderCam"]

bpy.data.scenes['Scene'].render.filepath = 'E:/Projects/blender/launch_script/image2.jpg'
bpy.ops.render.render(write_still=True)