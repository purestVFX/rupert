import take
take.append_to_sys("sqLib")
from sqLib.ftrackLib import connect
from sqLib.ftrackLib import utils as ftrackUtils
from sqLib.mayaLib import utils as mayaUtils
session = connect.get_session()
from sqLib.mayaLib import autoRender


projectID = '69b68ec6-ff3e-11e6-a7b6-22000ab8015b'

#q = "select parent.name, versions from Asset where parent.project_id is {} and type.name is 'Layout' and parent.parent.parent.name is '{}'".format(projectID, "Touch")
q = "select parent.name, versions from Asset where parent.project_id is {} and type.name is 'Layout'".format(projectID)

FTlayoutList = session.query(q).all()


def cameraPathFromShot(shot):
    for asset in shot["assets"]:
        if asset["type"]["name"] == "Camera" and len(asset["versions"]):
            camAV = asset["versions"][-1]
            return ftrackUtils.get_path_from_av(camAV, 'camera-abc', ftrackUtils.get_location(session))
    return None

noCameraList = ""

num_todo = 1000

for layout_asset in FTlayoutList:
    if len(layout_asset["versions"]) and num_todo:
        playableExists = False
        layoutAV = layout_asset["versions"][-1]
        for compo in layoutAV["components"]:
            if compo['name'] == 'ftrackreview-mp4':
                playableExists = True
                break

        if not playableExists:
            #print layout_asset["name"], layout_asset["parent"]["name"]
            layoutPath = ftrackUtils.get_path_from_av( layoutAV, 'layout-json', ftrackUtils.get_location(session))

            if layoutPath:
                layoutPath = layoutPath.replace('\\', '/')

                ff = layout_asset["parent"]["custom_attributes"]["fstart"]
                lf = layout_asset["parent"]["custom_attributes"]["fend"]

                frameRange = str(int(ff))+"-"+str(int(lf))

                cameraPath = cameraPathFromShot(layout_asset["parent"])

                shotName = layout_asset["parent"]["parent"]["name"]+"_"+layout_asset["parent"]["name"]

                if layoutPath and frameRange and cameraPath and num_todo:

                    print layoutPath, frameRange, cameraPath
                    audioPath = mayaUtils.get_audio_from_shot(layout_asset["parent"], session)
                    autoRender.render_layout_preview(cameraPath.replace("\\","/"), layoutPath.replace("\\","/"), layoutAV["id"], frameRange, shotName , audioPath)
                    num_todo = num_todo - 1
                elif layoutPath and not cameraPath:
                    noCameraList += shotName + "\n"


print "no camera list:"
print noCameraList