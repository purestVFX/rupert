import os
import json
import sys
#sys.path.append(r'C:\Users\clementp\dev\arcade')
#sys.path.append(r'C:\Users\clementp\dev\arcade_ftrack')

import arcade_ftrack


def getEpisodes(project):
    episodes = [child for child in project['children']
                if child['object_type']['name'] == 'Episode']
    return episodes


def getSequences(episode):
    sequences = [child for child in episode['children']
                 if child['object_type']['name'] == 'Sequence']
    return sequences


def getShots(sequence):
    shots = [child for child in sequence['children']
             if child['object_type']['name'] == 'Shot']
    return shots


def getPlateAssetVersion(shot):
    for asset in shot['assets']:
        #print asset['type']['name']
        if asset['type']['name'] == 'plate':
            return asset['versions'][-1]


ftb = arcade_ftrack.Bridge()
ftb.connect(url='https://dupevfx.ftrackapp.com',
            key='f7e328f2-9642-11e8-938c-0a580af4085d',
            user='clpo')

proj = ftb.session.get('Project', 'f67feb4a-96ff-11e8-8dff-0a580af4085d')
episodes = getEpisodes(proj)
for episode in episodes:
    sequences = getSequences(episode)
    for sequence in sequences:
        shots = getShots(sequence)
        for shot in shots:
            av = getPlateAssetVersion(shot)
            if av:
                for component in av["components"]:
                    if component["name"] == 'plate-dpx':
                        img_path = component['component_locations'][0]['resource_identifier']
                        output_path = os.path.join(os.path.dirname(img_path), 'playable.mp4')

                        command = 'ffmpeg -r 25 -y -f image2 -start_number 1001 -i {} ' \
                                '-vcodec h264 -pix_fmt yuv420p -r 25 {}'
                        command = command.format(img_path, output_path)
                        os.system(command)
                        print output_path

                        #job = av.encode_media(output_path)

                        server_location = ftb.session.query('Location where name is "ftrack.server"').one()

                        component = av.create_component(
                            path=output_path,
                            data={'name': "ftrackreview-mp4"},
                            location=server_location)

                        component['metadata']['ftr_meta'] = json.dumps({
                            'frameIn': 0,
                            'frameOut': 159,
                            'frameRate': 25
                        })

                        component.session.commit()

                        # job = ftb.session.encode_media(output_path)
                        # job_data = json.loads(job['data'])

                        # for output in job_data['output']:
                        #     component = ftb.session.get('FileComponent', output['component_id'])

                        #     # Add component to version.
                        #     component['version_id'] = av['id']

                        #     # Add thumbnail to the version.
                        #     if output['format'] == 'image/jpeg':
                        #         av['thumbnail_id'] = output['component_id']
                        #         av['asset']['parent']['thumbnail_id'] = output['component_id']

                        ftb.session.commit()
                        job = ftb.session.encode_media(component)
