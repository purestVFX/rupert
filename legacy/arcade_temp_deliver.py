
import sys

#sys.path.append("/home/ruth/dev/arcade/")

import os
import arcade
import glob
import clique

arcob = arcade.Arcade()

episode_id = 1

arcob.set_situation(episode_id, "episode")

count = 0

outdir = "/mnt/projects/1002_bountyhunters2/OUT/2018_08_23_delivery/"


for sequence in arcob.db.get_entities("sequence", episode_id):
    for shot in arcob.db.get_entities("shot", sequence["id"]):
        arcob.set_situation(shot["id"], "shot")
        path = arcob.fs.resolve_path("@shot")
        seq_shot = "BH_"+arcob.si["sequence"]+"_"+arcob.si["shot"]+"_CMP_V002"
        #print seq_shot,
        foundpath = glob.glob(path+"/work/*/?ende*/?002/")
        if foundpath:
            print "\n", seq_shot,
            count += 1
            if len(foundpath) > 1:
                print "########### dounble find ", seq_shot
	    #continue
            foundpath = foundpath[0]
            print foundpath
            found = False
            for file_sequence in clique.assemble(os.listdir(foundpath))[0]:
                print "   ", file_sequence
                found = True
            
            outlink_path = outdir + seq_shot
            
            if found and not os.path.exists(outlink_path):
                os.symlink(foundpath, outlink_path)

        else:
            pass
            #print "    missing"

print "\n\n", count, " matching dirs found"
