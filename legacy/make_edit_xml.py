import os
from ftrack_api import symbol

import take
take.append_to_sys("sqLib")
from sqLib.ftrackLib import connect
from sqLib.ftrackLib import utils as ftrackUtils
import urllib


xmlHead = """<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xmeml>
<xmeml version="4">
	<sequence id="sequence-2" TL.SQAudioVisibleBase="0" TL.SQVideoVisibleBase="0" TL.SQVisibleBaseTime="0" TL.SQAVDividerPosition="0.5" TL.SQHideShyTracks="0" TL.SQHeaderWidth="236" Monitor.ProgramZoomOut="1828915200000" Monitor.ProgramZoomIn="0" TL.SQTimePerPixel="0.048107761385503531" MZ.EditLine="1463132160000" MZ.Sequence.PreviewFrameSizeHeight="674" MZ.Sequence.PreviewFrameSizeWidth="1200" MZ.Sequence.AudioTimeDisplayFormat="200" MZ.Sequence.PreviewRenderingClassID="1297106761" MZ.Sequence.PreviewRenderingPresetCodec="1096172337" MZ.Sequence.PreviewRenderingPresetPath="EncoderPresets\SequencePreview\9678af98-a7b7-4bdb-b477-7ac9c8df4a4e\I-Frame Only MPEG.epr" MZ.Sequence.PreviewUseMaxRenderQuality="false" MZ.Sequence.PreviewUseMaxBitDepth="false" MZ.Sequence.EditingModeGUID="9678af98-a7b7-4bdb-b477-7ac9c8df4a4e" MZ.Sequence.VideoTimeDisplayFormat="101" MZ.WorkOutPoint="1828915200000" MZ.WorkInPoint="0" explodedTracks="true">
		<uuid>978ba062-69e7-48ac-9688-ecfd9971f2b3</uuid>
		<rate>
			<timebase>25</timebase>
			<ntsc>FALSE</ntsc>
		</rate>
		<name>test_xml_01</name>
		<media>
			<video>
				<format>
					<samplecharacteristics>
						<rate>
							<timebase>25</timebase>
							<ntsc>FALSE</ntsc>
						</rate>
						<codec>
							<name>Apple ProRes 422</name>
							<appspecificdata>
								<appname>Final Cut Pro</appname>
								<appmanufacturer>Apple Inc.</appmanufacturer>
								<appversion>7.0</appversion>
								<data>
									<qtcodec>
										<codecname>Apple ProRes 422</codecname>
										<codectypename>Apple ProRes 422</codectypename>
										<codectypecode>apcn</codectypecode>
										<codecvendorcode>appl</codecvendorcode>
										<spatialquality>1024</spatialquality>
										<temporalquality>0</temporalquality>
										<keyframerate>0</keyframerate>
										<datarate>0</datarate>
									</qtcodec>
								</data>
							</appspecificdata>
						</codec>
						<width>1200</width>
						<height>674</height>
						<anamorphic>FALSE</anamorphic>
						<pixelaspectratio>square</pixelaspectratio>
						<fielddominance>none</fielddominance>
						<colordepth>24</colordepth>
					</samplecharacteristics>
				</format>
                
                
				<track TL.SQTrackShy="0" TL.SQTrackExpandedHeight="25" TL.SQTrackExpanded="0" MZ.TrackTargeted="1">
    """

xmlTail = """
					<enabled>TRUE</enabled>
					<locked>FALSE</locked>
				</track>
                
			</video>
		</media>
		<timecode>
			<rate>
				<timebase>25</timebase>
				<ntsc>FALSE</ntsc>
			</rate>
			<string>00:00:00:00</string>
			<frame>0</frame>
			<displayformat>NDF</displayformat>
		</timecode>
		<labels>
			<label2>Forest</label2>
		</labels>
	</sequence>
</xmeml>"""

def shotText(sf, ef, scut, ecut, path, name, count):

    item = """					<clipitem id="clipitem"""+str(count)+"""">
						<masterclipid>masterclip-"""+str(count)+"""</masterclipid>
						<name>"""+name+"""</name>
						<enabled>TRUE</enabled>
						<rate>
							<timebase>25</timebase>
							<ntsc>FALSE</ntsc>
						</rate>
						<start>"""+str(scut)+"""</start>
						<end>"""+str(ecut)+"""</end>
						<in>"""+str(sf)+"""</in>
						<out>"""+str(ef)+"""</out>
						
						<file id="file-"""+str(count)+"""">
							<name>"""+name+"""</name>
							<pathurl>"""+path+"""</pathurl>
							<rate>
								<timebase>25</timebase>
								<ntsc>FALSE</ntsc>
							</rate>
							<timecode>
								<rate>
									<timebase>25</timebase>
									<ntsc>FALSE</ntsc>
								</rate>
								<string>00:00:00:00</string>
								<frame>0</frame>
							</timecode>
							<media>
								<video>
									<samplecharacteristics>
										<rate>
											<timebase>25</timebase>
											<ntsc>FALSE</ntsc>
										</rate>
										<width>1200</width>
										<height>674</height>
									</samplecharacteristics>
								</video>
							</media>
						</file>
					</clipitem>"""

    return item

######################################################################

######################################################################

def url_from_av(av, component_name ,session):
    server_location = session.get('Location', symbol.SERVER_LOCATION_ID)
    for component in av["components"]:
        if component["name"] == component_name:
            url = server_location.get_url(component)
            return url
            break
    return


def mov_from_shot(shot, session):
    for asset in shot["assets"]:
        if asset["type"]["name"] == "Layout" and len(asset["versions"]):
            av = asset["versions"][-1]

            playable = ftrackUtils.get_path_from_av(av, 'playable-mov', ftrackUtils.get_location(session))

            if not playable:
                url = url_from_av(av, "ftrackreview-mp4", session)
                if url:
                    print "downloading : ", url
                    location = ftrackUtils.get_location(session)
                    layoutPath = ftrackUtils.get_path_from_av(av,"layout-json",location)
                    path = layoutPath.replace(".json",".mov")
                    urllib.urlretrieve(url, path)
                    av.create_component(path, data={'name': "playable-mov"}, location=location)
                    session.commit()




            if playable:
                return playable

    return ""








from sqLib.mayaLib import utils as mayaUtils
session = connect.get_session()

projectID = '69b68ec6-ff3e-11e6-a7b6-22000ab8015b'
q = "select name, custom_attributes, assets.type.name, assets.versions from Shot where project_id is {} and parent.parent.name is '{}' order by name".format(projectID, "Cones")
shotList = session.query(q).all()


count = 0
markframe = 0
editxml= xmlHead

for aShot in shotList:

    if aShot["name"].isdigit():#count<10:

        print aShot["name"]
        ff = int(aShot["custom_attributes"]["fstart"])
        lf = int(aShot["custom_attributes"]["fend"])

        sf = ff-1001
        ef = lf-1000

        scut = sf+markframe
        ecut = ef+markframe
        markframe = ecut

        path = mov_from_shot(aShot, session).replace("\\","/").replace("O:","file://localhost/O%3a")

        if path:
            name = os.path.basename(path)
            cliptext = shotText(sf, ef, scut, ecut, path, name, count)
            editxml += cliptext


    count += 1

editxml += xmlTail

fh = open("C:/Phosphor_layout_edit.xml", "w")
fh.write(editxml)
fh.close()