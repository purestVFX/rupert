import subclass_test


class Child(subclass_test.Parent):
    def __init__(self):
        print "child init ran"
        super(Child, self).__init__()

    def arc_test(self):
        print

    def child_test(self):
        print "child_test ran\n"
        subclass_test.parent_print("from child object")

    def test(self):
        child_print("over ridden test funct")
        super(Child, self).test()


def child_print(a_string):
    print "##############"
    print "child printing function outside of class"
    print a_string
    print "##############"
        