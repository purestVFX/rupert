import os
import arcade
import arcade_ftrack
import clique
import shutil
import json

shotcount = 0

def ensureDir(path):
    adir = os.path.dirname(path)
    if not os.path.exists(adir):
        os.makedirs(adir)


task_list = ["comp", "conform"]

#footage_path = "/mnt/projects/1002_bountyhunters2/IN/20180724/"
footage_path = "/mnt/projects/1002_bountyhunters2/IN/20180730/"

project_name = "1002_bountyhunters2"
episode_name = "s2"

project_root = "/mnt/projects/1002_bountyhunters2/"

ftrack_project_id = "f67feb4a-96ff-11e8-8dff-0a580af4085d"

dirs = os.listdir(footage_path)
dirs.sort()

arc = arcade.Arcade()
ft_bridge = arcade_ftrack.Bridge(arc.situation)

ft_bridge.connect(url='https://dupevfx.ftrackapp.com', key="f7e328f2-9642-11e8-938c-0a580af4085d", user="ruth")

situ = arc.situation
situ["project"]=project_name
situ.at["project"]["ftrack_id"] = ftrack_project_id
situ["episode"]=episode_name
arc.fs.set_project_root(project_root)

for plate_dir in dirs:
    plate_path = os.path.join(footage_path, plate_dir)
    if os.path.isdir(plate_path):

        project_code, sequence, shot, plate_type, version = plate_dir.split("_")
        print project_code, sequence, shot, plate_type, version
        situ["sequence"] = sequence
        situ["shot"] = shot

        for taskname in task_list:
            situ["task"] = taskname
            print taskname
            arc.db.push_structure()        # push the situation into arcade db
            arc.fs.build_directories()     # create direcories on disk
            ft_bridge.push_structure()     # push situation into ftrack



            #get path to put plates
            
        
        situ['project_short_name']="BH"
        situ["padding"] = r"%04d"
        situ["version"] = "v001"
        situ["asset_type"] = "plate"
        situ["asset_name"] = plate_type
        situ["extension"] = "dpx"
        situ["file_name"] = arc.fs.resolve_name("file_name")

        print plate_path

        




       

##########################################
        ##publish to ftrack:
        ft_asset_type = ft_bridge.session.query('AssetType where name is plate').one()
        ftrack_shot = ft_bridge.session.get("Shot", situ.at["shot"]["ftrack_id"])
        ftrack_task = ft_bridge.session.get("Task", situ.at["task"]["ftrack_id"])

        query = 'Asset where name is "{}" and parent.id is {}'.format(plate_type, ftrack_shot['id'])
        ft_asset = ft_bridge.session.query(query).first()
        if not ft_asset:
            data = {
                'name': plate_type,
                'parent': ftrack_shot,
                'type': ft_asset_type
            }
            ft_asset = ft_bridge.session.create("Asset", data)
            ft_bridge.session.commit()

        ft_asset_version = ft_bridge.session.create('AssetVersion', {
                                    'asset': ft_asset, 'task': ftrack_task
                                })

        ft_bridge.session.commit()

        


        print "ft_asset_version: ", ft_asset_version["version"]
        situ["version"] = "v"+str(ft_asset_version["version"]).zfill(3)
        resolved_path = arc.fs.resolve_path("published_part_seq")

        component = ft_asset_version.create_component(resolved_path, data={'name': 'plate-dpx'}, location="auto")

        ft_bridge.session.commit()

        





        print resolved_path

        print "#################################"

        collection_src = clique.assemble(os.listdir(plate_path))[0][0]
        collection_src.head = os.path.join(plate_path, collection_src.head)
        print collection_src

        collection_dst = clique.parse(resolved_path+collection_src.format(" [{range}]"))

        print collection_dst

        component['metadata']['ftr_meta'] = json.dumps({
            'frameIn': int(collection_src.format("{range}").split("-")[0]),
            'frameOut': int(collection_src.format("{range}").split("-")[1]),
            'frameRate': 25
        })
        ft_bridge.session.commit()

        #print "clique_range:, ", int(collection_dst.format("{range}").split("-")[1])-1001

        ensureDir(resolved_path)

        for idx, aFile in enumerate(collection_src):
            shutil.copy(aFile, list(collection_dst)[idx])


        #### mp4 creation:

        mp4_output_path = os.path.join(os.path.dirname(resolved_path), 'playable.mp4')
        thum_output_path = os.path.join(os.path.dirname(resolved_path), 'thumnail.png')

        command = 'ffmpeg -r 25 -y -f image2 -start_number 1001 -i {} ' \
                                '-vcodec h264 -s 1920x1080 -pix_fmt yuv420p -r 25 {}'

        command = command.format(resolved_path, mp4_output_path)
        os.system(command)

        thum_cmd = "ffmpeg -i {} -vframes 1 {}".format(mp4_output_path, thum_output_path)
        os.system(thum_cmd)

        ###

        ft_asset_version.create_thumbnail(thum_output_path)
        ft_asset_version['task'].create_thumbnail(thum_output_path)


        server_location = ft_bridge.session.query('Location where name is "ftrack.server"').one()

        component = ft_asset_version.create_component(
            path=mp4_output_path,
            data={'name': "ftrackreview-mp4"},
            location=server_location)

        
        component['metadata']['ftr_meta'] = json.dumps({
            'frameIn': 0,
            'frameOut': int(collection_dst.format("{range}").split("-")[1])-1001,
            'frameRate': 25
        })

        component.session.commit()


        print "#################################"
        #this break is just for testing:
        #shotcount +=1
        #if shotcount >= 5:
        #    break
